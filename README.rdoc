

Meet Nik...


A born entrepreneur by age 3, Nik spends his nights and weekends as a Jedi Master with an invisible green lightsaber, Wooden Pizza Chef extraordinaire, and a Julliard trained Baa-rina. Nik’s favorite past time is imaginary play with his 6-year old son and 2-year old daughter. With a strong penchant (and stomach) for all things food, Nik loves going to new restaurants and encouraging his kids to try the most obscure items on the menu…look for his recommendations on the most interesting, family-friendly, haute cuisine in town.

Meet Soniya...


VP of Marketing at L’Oreal USA, Soniya’s favorite thing in life is the sweet sound of K & LJ’s giggles. Between strategy meetings and kids’ mealtimes, Soniya is obsessed with exploring the world (or at least, this city), hand in hand with her family. An arts workshop in the renovated wing of a museum, a playdate at a new playground or an intimately romantic dinner for her family of four, Soniya’s unique creative touch is infused into the whole family’s lifestyle…look out for her recommendations on the quirkiest products and activities for kids.

Meet K...


6 years and counting… Kaeden has mastered the art of monkey bars and the ability to transform his train tracks into roller coasters for the ‘rats that live under the station’.  Kaeden’s love of the city is infectious and reminds his parents daily why they chose to raise him here. Over the past year alone, Kaeden has written and performed in his first play on stage, starred in an HBO pilot and McDonalds commercial and has tutored his parents on the distinction of Calder’s later works from his earlier ones…look out for Kaeden as our resident toy blogger, specifically for kids aged 3-6.

Meet LJ...


2 years in development, she’s louder than times square on new year’s eve and a born politician. Lylah’s passions include dominating the playground circuit, joining in on street performances and taking long naps in her stroller where jack hammers are in full effect. And while she fears grass (she’s rarely seen it) and looks for airplanes when we go to the mall (we travel to other countries more than we travel to New Jersey), we believe she’ll find her destiny in this city. Demanding, opinionated and as vocal as a born New Yorker, look out for Lylah’s approval on…well, everything.
