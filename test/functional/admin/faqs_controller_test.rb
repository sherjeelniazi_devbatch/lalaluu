require 'test_helper'

class Admin::FaqsControllerTest < ActionController::TestCase
  setup do
    @admin_faq = admin_faqs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_faqs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_faq" do
    assert_difference('Admin::Faq.count') do
      post :create, admin_faq: {  }
    end

    assert_redirected_to admin_faq_path(assigns(:admin_faq))
  end

  test "should show admin_faq" do
    get :show, id: @admin_faq
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_faq
    assert_response :success
  end

  test "should update admin_faq" do
    put :update, id: @admin_faq, admin_faq: {  }
    assert_redirected_to admin_faq_path(assigns(:admin_faq))
  end

  test "should destroy admin_faq" do
    assert_difference('Admin::Faq.count', -1) do
      delete :destroy, id: @admin_faq
    end

    assert_redirected_to admin_faqs_path
  end
end
