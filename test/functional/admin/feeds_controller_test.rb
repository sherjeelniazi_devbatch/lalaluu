require 'test_helper'

class Admin::FeedsControllerTest < ActionController::TestCase
  setup do
    @admin_feed = admin_feeds(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_feeds)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_feed" do
    assert_difference('Admin::Feed.count') do
      post :create, admin_feed: {  }
    end

    assert_redirected_to admin_feed_path(assigns(:admin_feed))
  end

  test "should show admin_feed" do
    get :show, id: @admin_feed
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_feed
    assert_response :success
  end

  test "should update admin_feed" do
    put :update, id: @admin_feed, admin_feed: {  }
    assert_redirected_to admin_feed_path(assigns(:admin_feed))
  end

  test "should destroy admin_feed" do
    assert_difference('Admin::Feed.count', -1) do
      delete :destroy, id: @admin_feed
    end

    assert_redirected_to admin_feeds_path
  end
end
