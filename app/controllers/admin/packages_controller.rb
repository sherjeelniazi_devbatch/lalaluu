class Admin::PackagesController < Admin::AdminsController

	def index
		@packages = Package.order('name ASC').page params[:page]
    	authorize! :read, @packages
	end
	
	def show
    @package = Package.find(params[:id])
    authorize! :read, @package
  end

  def new
    @package = Package.new
    authorize! :create, @package
  end
  
  def create
    @package = Package.new(permitted_params.for_package)
    authorize! :create, @package
    if @package.save
      redirect_to admin_packages_path, notice: "Package has been created."
    else
      render 'new'
    end
  end

  def edit
    @package = Package.find(params[:id])
    authorize! :update, @package
  end
  
  def update
    @package = Package.find(params[:id])
    authorize! :update, @package
    if @package.update_attributes(permitted_params.for_package)
      redirect_to admin_packages_path, notice: 'Package was successfully updated.'
    else
      render action: "edit"
    end
  end
  
  def destroy
    @package = Package.find(params[:id])
    authorize! :destroy, @package
    @package.destroy
    redirect_to admin_packages_url
  end
end