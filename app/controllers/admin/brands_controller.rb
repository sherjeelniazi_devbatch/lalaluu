class Admin::BrandsController < Admin::AdminsController
	def index
		@brands = Brand.order('name ASC').page params[:page]
    authorize! :read, @brands
	end
	
	def show
    @brand = Brand.find(params[:id])
    authorize! :read, @brand
  end

  def new
    @brand = Brand.new
    authorize! :create, @brand
  end
  
  def create
    @brand = Brand.new(permitted_params.for_brand)
    authorize! :create, @brand
    if @brand.save
      redirect_to admin_brands_path, notice: "Brand has been created."
    else
      render 'new'
    end
  end

  def edit
    @brand = Brand.find(params[:id])
    authorize! :update, @brand
  end
  
  def update
    @brand = Brand.find(params[:id])
    authorize! :update, @brand
    if @brand.update_attributes(permitted_params.for_brand)
      redirect_to admin_brand_path, notice: 'Brand was successfully updated.'
    else
      render action: "edit"
    end
  end
  
  def destroy
    @brand = Brand.find(params[:id])
    authorize! :destroy, @brand
    @brand.destroy
    redirect_to admin_brands_url
  end

end