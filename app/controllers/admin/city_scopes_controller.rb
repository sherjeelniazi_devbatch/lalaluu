class Admin::CityScopesController < ApplicationController
    
  def index
    @city_scopes = CityScope.find(:all)
    #@city_scopes = CityScope.find(:all)
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @city_scopes }
    end
  end

  # GET /uploads/1
  # GET /uploads/1.json
  def show
    @city_scope = CityScope.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @city_scope }
    end
  end

  # GET /uploads/new
  # GET /uploads/new.json
  def new
    @city_scope = CityScope.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @city_scope }
    end
  end

  def edit
    @city_scope = CityScope.find(params[:id])
  end

  def create
    @city_scope = CityScope.new(:upload=>params[:city_scope][:upload],:redirect_url=>params[:city_scope][:redirect_url])

    if @city_scope.save
      redirect_to admin_city_scopes_path, notice: "City Scope has been created."
    else
      render 'new'
    end
  end

  def update
    @city_scope = CityScope.find(params[:id])

    respond_to do |format|
      if @city_scope.update_attributes(params[:upload])
        format.html { redirect_to @city_scope, notice: 'Upload was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @city_scope.errors, status: :unprocessable_entity }
      end
    end
  end
  def destroy
    @city_scope = CityScope.find(params[:id])
    authorize! :destroy, @city_scope
    @city_scope.destroy
    respond_to do |format|
      format.html { redirect_to admin_city_scopes_url }
      format.json { head :no_content }
    end
  end
  
end
