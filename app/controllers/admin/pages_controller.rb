class Admin::PagesController < ApplicationController
	before_filter :get_users_and_categories, only: [ :new, :create, :edit, :update ]

	def index
		@pages = Page.includes(:category).order('category_id ASC').page(params[:page])
    	authorize! :read, @pages
	end

	def blogs
		@pages = Page.where(category_id: Category.find_by_slug('blog').id).page(params[:page])
    	authorize! :read, @pages
    	render :index
	end

	def new
		@page = Page.new
   		authorize! :create, @page
	end

	def create
		@page = Page.new(permitted_params.for_page)
	  	authorize! :create, @page
	    if @page.save
	      	redirect_to :action => :index
	    else 
	    	redirect_to :action => :new	
	    end  
		end

	def edit
	    @page = Page.find_by_slug(params[:id])
	  	authorize! :update, @page
		end

		def update
			@page = Page.find_by_slug(params[:id])
	    	authorize! :update, @page
		    
		    if @page.update_attributes(permitted_params.for_page)
		    	flash[:notice] = "Page updated successfully."
	      	redirect_to :action => :index 
		    end

	end

	def destroy
		@page = Page.find_by_slug(params[:id])
    	authorize! :destroy, @page
		if @page.delete
			flash[:alert] = "Page deleted successfully."
    	redirect_to :action => :index
		end
	end


private ###------------------------------------------------------

def get_users_and_categories
	@users = User.includes(:profile).all
	@categories = Category.all(:order => 'name')
end

end
