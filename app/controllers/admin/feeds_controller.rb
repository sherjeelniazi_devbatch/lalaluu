class Admin::FeedsController < Admin::AdminsController
  def index
    @feeds = Feed.order('created_at ASC').page( params[:page] )
    authorize! :read, @feeds
  end

  def show
    @feed = Feed.find(params[:id])
    authorize! :read, @feed
  end

  def new
    @feed = Feed.new
    authorize! :create, @feeds
  end

  def edit
    @feed = Feed.find(params[:id])
    authorize! :update, @feed
  end

  def create
    @feed = Feed.new(permitted_params.for_feed)
    authorize! :create, @feed

    if @feed. save
      redirect_to admin_feed_path(@feed), notice: 'Feed was successfully created.'
    else
      render "new"
    end
  end

  def update
    @feed = Feed.find(params[:id])
    authorize! :update, @feed

    if @feed.update_attributes(permitted_params.for_feed)
      redirect_to admin_feed_path(@feed), notice: 'Feed was successfully updated.'
    else
      render "edit"
    end
  end

  def destroy
    @feed = Feed.find(params[:id])
    authorize! :destroy, @feed
    @feed.destroy
    redirect_to admin_feeds_path
  end
end
