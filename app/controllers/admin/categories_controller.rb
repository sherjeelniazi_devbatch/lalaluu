class Admin::CategoriesController < ApplicationController

	def index
		@categories = Category.all
    authorize! :read, @categories
	end

	def show
		@Category = Category.find(params[:id])
    authorize! :read, @category
		respond_to do |format|
			format.html # show.html.erb
			format.json { render json: @category }
		end
	end


	def new
		@category = Category.new
    authorize! :create, @category

		respond_to do |format|
			format.html # new.html.erb
			format.json { render json: @category }
		end
	end

	def create
		@category = Category.new(permitted_params.for_category)
    authorize! :create, @category
		if @category.save
			redirect_to admin_categories_path
		else
			render 'new'
		end

	end

	def edit
		@category = Category.find(params[:id])
    authorize! :update, @category
	end

	def update
		@category = Category.find(params[:id])
    authorize! :update, @category
		respond_to do |format|
			if @category.update_attributes(permitted_params.for_category)
				format.html { redirect_to admin_categories_path}
				format.json { head :no_content }
			end
		end
	end

	def destroy
		@category = Category.find(params[:id])
    authorize! :destroy, @category
		@category.destroy
		respond_to do |format|
			format.html { redirect_to admin_categories_url }
			format.json { head :no_content }
		end
	end	
end
