class Admin::CouponsController < Admin::AdminsController

  def index
    @coupons = Coupon.order('created_at ASC').page( params[:page] )
    authorize! :read, @coupons
  end

  def show
    @coupon = Coupon.find(params[:id])
    authorize! :read, @coupon
  end

  def new
    @coupon = Coupon.new
    authorize! :create, @coupon
  end

  def edit
    @coupon = Coupon.find(params[:id])
    authorize! :update, @coupon
  end

  def create
    @coupon = Coupon.new(permitted_params.for_coupon)
    authorize! :create, @coupon

    if @coupon. save
      redirect_to admin_coupon_path(@coupon), notice: 'Feed was successfully created.'
    else
      render "new"
    end
  end

  def update
    @coupon = Coupon.find(params[:id])
    authorize! :update, @coupon

    if @coupon.update_attributes(permitted_params.for_coupon)
      redirect_to admin_coupon_path(@coupon), notice: 'Feed was successfully updated.'
    else
      render "edit"
    end
  end

  def destroy
    @coupon = Coupon.find(params[:id])
    authorize! :destroy, @coupon
    @coupon.destroy
    redirect_to admin_coupons_path
  end
end