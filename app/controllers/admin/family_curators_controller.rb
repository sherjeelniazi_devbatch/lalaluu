class Admin::FamilyCuratorsController < ApplicationController

	def index
		@curators = FamilyCurator.page(params[:page])
    	authorize! :read, @curators
	end

	def new
		@curator = FamilyCurator.new
    	authorize! :create, @curator
	end

	def create
		@curator = FamilyCurator.new(permitted_params.for_family_curator)
   		authorize! :create, @curator
	    if @curator.save
	      	redirect_to :action => :index
	    else 
	    	redirect_to :action => :new	
	    end  
	end

	def edit
	    @curator = FamilyCurator.find(params[:id])
    	authorize! :update, @curator
	end

	def update
		@curator = FamilyCurator.find(params[:id])
    	authorize! :update, @curator
	    
	    if @curator.update_attributes(permitted_params.for_family_curator)
	    	flash[:notice] = "Family Curator updated successfully."
	      	redirect_to :action => :index 
	    end

	end

	def destroy
		@curator = FamilyCurator.find_by_slug(params[:id])
    	authorize! :destroy, @curator
		if @curator.delete
			flash[:alert] = "Family Curator deleted successfully."
	      	redirect_to :action => :index
		end
	end


end
