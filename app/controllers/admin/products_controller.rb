class Admin::ProductsController < Admin::AdminsController

	def index
		@products = Product.includes(:brand).order('name ASC').page params[:page]
    authorize! :read, @products
	end
	
	def show
    @product = Product.find(params[:id])
    authorize! :read, @product
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @product }
    end
  end


  def new
    @product = Product.new
    authorize! :create, @product

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @product }
    end
  end
  def create
    @product = Product.new(permitted_params.for_product)
    if @product.save
      redirect_to admin_products_path, notice: "Product has been created."
    else
      render 'new'
    end
  end

  def edit
    @product = Product.find(params[:id])
    authorize! :update, @product
  end
  
  def update
    @product = Product.find(params[:id])
    authorize! :read, @product

    respond_to do |format|
      if @product.update_attributes(permitted_params.for_product)
        format.html { redirect_to admin_products_path, notice: 'Product was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @product = Product.find(params[:id])
    authorize! :destroy, @products
    @product.destroy
    respond_to do |format|
      format.html { redirect_to admin_products_url }
      format.json { head :no_content }
    end
  end
end