class Admin::BoxesController < Admin::AdminsController
  before_filter :get_products
	
  def index
		@boxes = Box.all
    authorize! :read, @boxes
	end
	
	def show
    @box = Box.find(params[:id])
    authorize! :read, @box
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @box }
    end
  end

  def get_products
    @agegroup = Agegroup.all
    @products = Product.all
  end

  def new
    @box = Box.new
    authorize! :create, @box

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @box }
    end
  end
  def create
    @box = Box.new(permitted_params.for_box)
    authorize! :create, @box
    if @box.save
      redirect_to admin_boxes_path, notice: "Box has been created."
      
    else
      render 'new'
    end
 
  end
  def edit
    @box = Box.find(params[:id])
    @box_products = @box.products 
    authorize! :update, @box
  end
  
  def update
    @box = Box.find(params[:id])
    params[:box][:product_ids] ||= []
    authorize! :update, @box
    respond_to do |format|
      if @box.update_attributes(permitted_params.for_box)
        format.html { redirect_to admin_boxes_path, notice: 'Box was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @box.errors, status: :unprocessable_entity }
      end
    end
  end
  def destroy
    @box = Box.find(params[:id])
    authorize! :destroy, @box
    @box.destroy
    respond_to do |format|
      format.html { redirect_to admin_boxes_url }
      format.json { head :no_content }
    end
  end
end
