class Admin::AdminsController < ApplicationController
	# methods only for admin section goes here

  before_filter :authenticate_admin! 

private ##################

	def get_user(id)
		@user = User.get_spouse_and_children.find(id)
	end

	def authenticate_admin!
		redirect_to root_url, alert: 'Not Authorized!' unless current_user && current_user.admin?
	end
end