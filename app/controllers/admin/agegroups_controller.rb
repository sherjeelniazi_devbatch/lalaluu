class Admin::AgegroupsController < Admin::AdminsController
 
  def index
		@agegroups = Agegroup.order('group_name ASC').page params[:page]
    authorize! :read, @agegroups
	end
	
	def show
    @agegroup = Agegroup.find(params[:id])
    authorize! :read, @agegroup
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @agegroup }
    end
  end


  def new
    @agegroup = Agegroup.new
    authorize! :create, @agegroup

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @agegroup }
    end
  end
  def create
    @agegroup = Agegroup.new(permitted_params.for_agegroup)
    if @agegroup.save
      redirect_to admin_agegroups_path, notice: "Age Group has been created."
    else
      render 'new'
    end
  end

  def edit
    @agegroup = Agegroup.find(params[:id])
    authorize! :update, @agegroup
  end
  
  def update
    @agegroup = Agegroup.find(params[:id])
    authorize! :read, @agegroup

    respond_to do |format|
      if @agegroup.update_attributes(permitted_params.for_agegroup)
        format.html { redirect_to admin_agegroups_path, notice: 'Age Group was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @agegroup.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    binding.pry
    @agegroup = Agegroup.find(params[:id])
    authorize! :destroy, @agegroups
    @agegroup.destroy
    respond_to do |format|
      format.html { redirect_to admin_agegroups_url }
      format.json { head :no_content }
    end
  end
end
