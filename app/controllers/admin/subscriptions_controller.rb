class Admin::SubscriptionsController < ApplicationController
  def index
    @subscriptions = Subscription.order('subscription ASC').page params[:page]
    authorize! :read, @subscriptions
  end
  
  def new
    @subscription = Subscription.new
    authorize! :create, @subscription

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @subscription }
    end
  end
  
  def create
    @subscription = Subscription.new(permitted_params.for_subscription)
    if @subscription.save
      redirect_to admin_subscriptions_path, notice: "Subscription has been created."
    else
      render 'new'
    end
  end

  def edit
    @subscription = Subscription.find(params[:id])
    authorize! :update, @subscription
  end
  
  def update
    @subscription = Subscription.find(params[:id])
    authorize! :read, @subscription

    respond_to do |format|
      if @subscription.update_attributes(permitted_params.for_subscription)
        format.html { redirect_to admin_subscriptions_path, notice: 'Product was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @subscription.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @subscription = Subscription.find(params[:id])
    authorize! :destroy, @subscriptions
    @subscription.destroy
    respond_to do |format|
      format.html { redirect_to admin_subscriptions_url }
      format.json { head :no_content }
    end
  end
  
  
	def show
    @subscription = Subscription.find(params[:id])
    authorize! :read, @subscription
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @subscription }
    end
  end
  
end
