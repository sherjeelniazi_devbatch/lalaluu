class Admin::SettingsController < Admin::AdminsController

  def index
    @set = Setting.first
    authorize! :read, @set
  end

  def update
    @set = Setting.first
    authorize! :update, @set
    
    if @set.update_attributes(permitted_params.for_setting)
      redirect_to admin_settings_path, notice: 'Setting was successfully updated.'
    end
    
  end

end
