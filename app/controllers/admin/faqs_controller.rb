class Admin::FaqsController < Admin::AdminsController
  def index
    @faqs = Faq.order('created_at ASC').page( params[:page] )
    authorize! :read, @faqs
  end

  def show
    @faq = Faq.find(params[:id])
    authorize! :read, @faq
  end

  def new
    @faq = Faq.new
    authorize! :create, @faq
  end

  def edit
    @faq = Faq.find(params[:id])
    authorize! :update, @faq
  end

  def create
    @faq = Faq.new(permitted_params.for_faq)
    authorize! :create, @faq

    if @faq.save
      redirect_to admin_faq_path(@faq), notice: 'Faq was successfully created.'
    else
      render action: "new"
    end
  end

  def update
    @faq = Faq.find(params[:id])
    authorize! :update, @faq

    if @faq.update_attributes(permitted_params.for_faq)
      redirect_to admin_faq_path(@faq), notice: 'Faq was successfully updated.'
    else
      render action: "edit"
    end
  end

  def destroy
    @faq = Faq.find(params[:id])
    authorize! :destroy, @faq
    @faq.destroy
    redirect_to faqs_url
  end

end
