class Admin::SpousesController < Admin::AdminsController

	def show
		@user = get_user(params[:user_id])
		authorize! :read, @user
	end

	def edit
		render text: 'Edit'
	end

	def update
		
	end

	def destroy
		render text: 'Destroy'
	end
end