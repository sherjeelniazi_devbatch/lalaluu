class Admin::OrdersController < Admin::AdminsController

	def index
		@orders = UserSubscription.includes(:user, :package, :box, line_items: [:agegroup]).find(:all, :order =>"id Desc" )
        authorize! :read, @orders
        respond_to do |format|
		    format.html
		    format.csv { send_data UserSubscription.where(:paymentstatus => true).to_csv }
		end
	end
	
	def toggled_status
	    @order = UserSubscription.find(params[:id])
	    authorize! :update, @order
	    @order.paymentstatus = !@order.paymentstatus?
	    @order.save!
	    redirect_to admin_orders_path
	end 

	def show
		@order = UserSubscription.find(params[:id])
    	authorize! :read, @order
		@stripe_charge = @order.stripe_charge
	end
end