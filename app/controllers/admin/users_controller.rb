class Admin::UsersController < Admin::AdminsController

	def	index
		@users = User.includes(:profile).order('profiles.first_name ASC').page(params[:page])
		authorize! :read, @users
	end

	def new
		@user = User.new
		authorize! :manage, @user
		@user.build_profile
	end

	def create
		@user = User.new(permitted_params.for_user)
		authorize! :manage, @user
		if @user.save
			redirect_to admin_users_path
		else
			render 'new'
		end
	end

	def edit
		@user = User.find(params[:id])
		authorize! :manage, @user
	end

	def update
		@user = User.find(params[:id])
		authorize! :manage, @user

		if @user.update_attributes(permitted_params.for_user)
			flash[:notice] = 'User Successfully Updated'.concat( @user.email == params[:user][:email] ? '' : ' - A Confirmation email has been sent!' )
			redirect_to admin_users_path
		else
			render 'edit'
		end
	end

	def show
		id = params[:profile] == 'my' ? current_user.id : params[:id]
		@user = User.find(id)
		authorize! :read, @user
		render :edit
	end

	def destroy
		user = User.find(params[:id])
		authorize! :manage, user
		user.destroy
		redirect_to admin_users_path, notice: 'User Successfully Deleted!'
	end
end