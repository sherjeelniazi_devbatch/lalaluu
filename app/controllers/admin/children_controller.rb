class Admin::ChildrenController < Admin::AdminsController

	def show
		@user = get_user(params[:user_id])
		@child = @user.children.detect { |c| c.id == params[:id].to_i}
    	authorize! :read, @user
	end
end