class Users::SessionsController < Devise::SessionsController

	def new
		@user =
		if session[:invitation]
			invitation = Invitation.find(session[:invitation])
			User.new(email: invitation.friend_email)
		else
			User.new
		end
		@user.build_profile
	end

end