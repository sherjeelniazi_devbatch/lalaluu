class Users::SpousesController < ApplicationController

	before_filter :get_current_user, except: [:destroy]

	def new
		if @user_current.spouse
			redirect_to edit_users_spouse_path(@user_current)
		else
			@spouse = Spouse.new
			@user_current.build_profile unless @user_current.profile
		end
	end

	def create
		@spouse = @user_current.build_spouse(permitted_params.for_spouse)
		if @spouse.save
			redirect_to new_users_child_path
		else
			render 'new'
		end
	end

	def edit
		@spouse = @user_current.spouse
		@user_current.build_profile unless @user_current.profile
	end

	def update
		@spouse = @user_current.spouse
		
		if @spouse.update_attributes(permitted_params.for_spouse)
			redirect_to new_users_child_path
		else
			render 'edit'
		end
	end

	def save_avatar
		if @user_current.update_attributes(permitted_params.for_user)
			redirect_to :back
		else
			redirect_to :back, alert: 'Avatar upload failed only (jpg jpeg png) allowed!'
		end
	end

end