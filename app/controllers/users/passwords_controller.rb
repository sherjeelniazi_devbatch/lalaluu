class Users::PasswordsController < Devise::PasswordsController
  def resource_params
    permitted_params.for_devise_password
  end
  private :resource_params
end