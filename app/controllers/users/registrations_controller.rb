class Users::RegistrationsController < Devise::RegistrationsController
	# OVERIDE DEVISE METHODS -----------------------------------#
	
	def create	
		build_resource
		@user = resource
		if session[:invitation]
			invitation = Invitation.find(session[:invitation]) 
			if @user.email == invitation.friend_email
				@user.skip_confirmation! 
				@user.confirmed_at = DateTime.now
				# ADD WELCOME EMAIL IF NEEDED -- HERE --
			end
		end

		if @user.save
			session[:registering_user] = @user.id
			if session[:invitation]
				sender = invitation.sender
				@user.update_attribute(:invitation_id, session[:invitation])
				sender.friendships.create!(friend_id: @user.id)
				session[:invitation] = nil
			end
			redirect_to new_users_spouse_path
		else
			render 'devise/sessions/new'
		end
	end

	def edit
		resource.build_spouse unless resource.spouse
		resource.build_favourite unless resource.favourite
    	render layout: 'user'
	end

	def update
		resource.build_favourite unless resource.favourite
		resource.build_spouse unless resource.spouse
			
		self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
		prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)

		if resource.update_without_password(resource_params)
			sign_in resource_name, resource, :bypass => true
			redirect_to edit_user_registration_path, notice: 'Profile updated successfully!'
		else
			render 'edit'
		end
	end

private #-------------------------------------------------------------
  def resource_params
  	permitted_params.for_devise_registration
  end

  def after_sign_up_path_for(resource)
    authors_waiting_path
  end

  def after_inactive_sign_up_path_for(resource)
    root_url
  end
end