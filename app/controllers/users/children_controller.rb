class Users::ChildrenController < ApplicationController
	before_filter :get_current_user


	def new	
		@user = @user_current		
		redirect_to edit_users_child_path(@user) unless @user.children.blank?
		4.times { @user.children.build }
		# create_favourites
		# IS COMMENTED BECAUSE BEFORE ONE FAVOURITE FIELD WAS ON CHILDREN PAGE
		# UNCOMMENT IF WANT THE SAME AGAIN!!!
	end

	def edit
		@user = @user_current
		# create_favourites
	end

	def update
		params = permitted_params.for_user
		#params.delete(:favourite_attributes) if params[:favourite_attributes][:kid_products].blank?

		@user = @user_current
		if @user.update_attributes(params)
			redirect_to new_users_favourite_path
		else
			# create_favourites
			render 'edit'
		end
	end

	# def destroy
	# 	child = current_user.children.find(params[:id])
	# 	child.destroy
	# 	redirect_to edit_profile_user_path(current_user), notice: 'Child successfully deleted!'
	# end

end