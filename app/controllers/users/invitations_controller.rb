class Users::InvitationsController < ApplicationController
	# before_filter :authenticate_user!

	def new
		if current_user.present?
			@invitation = current_user.invitations.build
		else 
			redirect_to root_path, :notice => "Please login to invite friends"
		end
	end

	def create
		@invitation = current_user.invitations.new(permitted_params.for_invitation)
		@invitation.verify_data # VERIFY DATA AND SET/FIND valid_objs and invalid_objs

		if !@invitation.invalid_objs.blank?
			prepare_error_message()			
		elsif !@invitation.valid_objs.blank?
			save_and_send_mail()
			flash[:notice] = 'Your friends are now invited!'
		end
		redirect_to new_users_invitation_path
	end

	def generate_token
		invitation = Invitation.new
		invitation.save_for_api(current_user,select_api)
		
		render json: invitation.token.to_json
	end

private #------------------------------
	
	def save_and_send_mail
		@invitation.valid_objs.each do |obj|
			obj.save!
			Mailer.invite_friends(current_user, obj).deliver
		end		
	end

	def prepare_error_message
		@invitation = @invitation.invalid_objs.first
		@invitation.valid?
		@invitation.friend_email = params[:invitation][:friend_email]
		render 'new'
		return		
	end

	def select_api
		%(facebook twitter).include?(params[:api]) ? params[:api] : 'face-ter'
	end
end