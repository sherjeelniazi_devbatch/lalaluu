class Users::FavouritesController < ApplicationController
	before_filter :get_current_user

	def kid_products
		@kid_products = FavouriteKidProduct.order(:name)
		respond_to do |format|
			format.html
			format.json { render json: @kid_products.tokens(params[:q]) }
		end
	end

	def nyc_restaurants
		@nyc_restaurants = NycRestaurant.order(:name)
		respond_to do |format|
			format.html
			format.json { render json: @nyc_restaurants.tokens(params[:q]) }
		end
	end

	def nyc_playgrounds
		@nyc_playgrounds = NycPlayground.order(:name)
		respond_to do |format|
			format.html
			format.json { render json: @nyc_playgrounds.tokens(params[:q]) }
		end
	end

	def nyc_spots
		@nyc_spots = NycSpot.order(:name)
		respond_to do |format|
			format.html
			format.json { render json: @nyc_spots.tokens(params[:q]) }
		end
	end

	def new
		@user = @user_current
		redirect_to edit_users_favourite_path(@user) unless @user.favourite.blank?
		create_favourites
	end

	def edit
		@user = @user_current
	end

	def update
		@user = @user_current
		params = permitted_params.for_user
		fav_attr = params[:favourite_attributes]
		# params.delete(:favourite_attributes) if fav_attr[:nyc_restaurants].blank? && \
		# 										fav_attr[:nyc_playgrounds].blank? && \
		# 										fav_attr[:nyc_spots].blank?
		if @user.update_attributes(params)
			if @user.confirmed?
				flash[:notice] = 'Signed Up and Signed In!' 
				sign_in_and_redirect(:user, @user)
			else
				redirect_to root_url, notice: 'Welcome to LaLaaLu. Please confirm your account to continue.'
			end
		else
			render 'new'
		end
	end

end