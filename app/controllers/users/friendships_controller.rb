class Users::FriendshipsController < ApplicationController
  before_filter :authenticate_user!#, except: [:new, :send_message]
  layout 'user'

  def index
    @users = User.includes(:profile).order('profiles.first_name').reject { |u| u == current_user }
    @friends = current_user.friends.to_a
    @inverse_friends = current_user.inverse_friends.to_a
    @pending_friends = current_user.pending_friends.to_a
    @inverse_pending_friends = current_user.inverse_pending_friends.to_a
  end

	def new
		invitation = Invitation.find_by_token(params[:token])
		session[:invitation] = invitation.id
		redirect_to new_user_session_path, notice: 'Welcome to Lalaalu. Go ahead and Sign Up / Log In to recieve our exciting offers'
	end
	
  def create
    new_friend = User.find(params[:friend_id])
    @friendship = current_user.friendships.build(friend_id: new_friend.id)

    if @friendship.save
      redirect_to :back, notice: 'Friend Request Sent!'
    else
      redirect_to :back, error: 'Unable to add friend.'
    end
  end

  def update
    friend = User.find(params[:friend_id])
    friendships = current_user.inverse_friendships.where(user_id: friend.id).first

    if friendships.approve
      redirect_to :back, notice: 'Friend Approved!'
    else
      redirect_to :back, error: 'Unable to approved friend!'
    end 
  end

  def compose_message
    puts "#{current_user.inspect}------------------------------current user---"
    @friend = User.find(params[:id])

  end


end