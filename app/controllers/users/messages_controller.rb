class Users::MessagesController < ApplicationController
	before_filter :authenticate_user!
  skip_before_filter :verify_authenticity_token, :only => [:send_message]

	def index
    @user1 = User.find(current_user.id)
		@messages = 
			case params[:tab]
				when 'inbox' then @user1.inbox
				when 'sent'  then @user1.outbox
				when 'trash'
          @user1.trash
				else; @user1.inbox
      end
    case params[:tab]
      when 'inbox' then @margin = "140px"
      when 'sent'  then @margin = "140px"
      when 'trash'
        @margin = "12px"
      else; @margin = "140px"
    end
    render layout: 'user'
	end

	def new
    @friend = User.find(params[:friend_id])
    render layout: 'user'
	end

  def send_message

    @user1 = User.find(params[:send_user_id])  # => create first user
    @user2 = User.find(params[:friend_id])   # => return the second user

    @user1.send_message(params[:subject],params[:message],@user2)

    redirect_to users_messages_path, :notice => 'Message Sent!'

  end

	def show
    @user1 = User.find(current_user.id)

    case params[:tab]
      when 'inbox' then
        @message = @user1.inbox.find(params[:id])
        @user1.inbox.find(params[:id]).mark_as_read
      when 'sent'  then @message = @user1.outbox.find(params[:id])
      when 'trash'
        @message = @user1.trash.find(params[:id])
      else;
      @message = @user1.inbox.find(params[:id])
      @user1.inbox.find(params[:id]).mark_as_read
    end
    render layout: 'user'
	end

  def del_msg
    @user1 = User.find(current_user.id)

    case params[:tab]
      when 'inbox' then
        @message = @user1.inbox.find(params[:id])
        @message.delete
      when 'sent'  then
        @message = @user1.outbox.find(params[:id])
        @message.delete
      when 'trash'
        @user1.trash.find(params[:id])
        @message.delete
      else;
      @message = @user1.inbox.find(params[:id])
      @message.delete
    end
    redirect_to :back, notice: 'Message Deleted!'
  end

  def back_inbox
    @user1 = User.find(current_user.id)
    @message = @user1.trash.find(params[:id])
    @message.undelete
    redirect_to :back, notice: 'Message moved to inbox!'
  end

  def del_all_msgs
    @user = User.find(current_user.id)
    case params[:tab]
      when 'inbox' then
        @user.empty_mailbox(:inbox => true)
      when 'sent'  then
        @user.empty_mailbox(:outbox => true)
      when 'trash'
        @user.empty_mailbox(:trash => true)
      else;
      @user.empty_mailbox(:inbox => true)
    end
    redirect_to :back, notice: 'Messages Deleted!'
  end
end