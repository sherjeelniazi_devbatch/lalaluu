class Users::BoxesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :get_current_user
  layout 'user'
  def show

    @box = Box.find(params[:id])
    @products = @box.products
  end
end
