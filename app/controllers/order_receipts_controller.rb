class OrderReceiptsController < ApplicationController
  
  before_filter :authenticate_user!

  def index
  end

  def show

  	@order = current_user.user_subscriptions.find(params[:id])
  	@billing = Stripe::Token.retrieve(@order.card_token)
  end

end
