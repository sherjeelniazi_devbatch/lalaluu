class TinymceAssetsController < ApplicationController
  def create
    attachment = Attachment.new
    attachment.image =  params[:file]
    attachment.save!

    render json: {
      image: {
        url: attachment.image.url
      }
    }, content_type: "text/html"
  end
end