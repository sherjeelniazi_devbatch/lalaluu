class UserSubscriptionsController < ApplicationController
  
  before_filter :authenticate_user!

  def new  
    @user_subscription = current_user.user_subscriptions.find(params[:id])

  end
  
  def show
    @user_subscription = current_user.user_subscriptions.find(params[:id])
     # :include => [:agegroup]
  end
  
  def edit
    @user_subscription = current_user.user_subscriptions.find(params[:id])
    @kidbox = @user_subscription.box
    @agegroup = @kidbox.agegroups
  end
  
  def index
    
  end

  def create
    @package = Package.find(params[:package])
    @subscriptions = params[:subscription]
    @total_boxes = @subscriptions.values.map {|s| s.to_f }.inject(:+)
    @total_price =  @total_boxes * @package.price

    if session[:subscription_id].present?
      @user_subscription = current_user.user_subscriptions.find(session[:subscription_id])
    else
      @user_subscription = current_user.user_subscriptions.build
      @user_subscription.save!
      session[:subscription_id] = @user_subscription.id.to_s
    end    
    #updating the cart
    # @user_subscription.line_items.where(:gift_it => params[:gift] == "true" ? true : false).delete_all
    @subscriptions.each do |li|
      if li.last.to_i > 0
        @line_item = @user_subscription.line_items.build
        if params[:gift] == "true"
          @line_item.update_attributes(:gift_it => true, 
                                       :agegroup_id => li.first, 
                                       :qty => li.last, 
                                       :package_id => @package.id, 
                                       :price => li.last.to_i * @package.price)
        else
          @line_item.update_attributes(:gift_it => false, 
                                       :agegroup_id => li.first, 
                                       :qty => li.last, :package_id => @package.id, 
                                       :price => li.last.to_i * @package.price)
        end
      end
    end
    if params[:rec_email] && params[:rec_address]
      @gifted_rec = @user_subscription.build_recipient_detail
      @gifted_rec.update_attributes(:name => params[:rec_name], 
                                    :message => params[:rec_message],
                                    :email => params[:rec_email], 
                                    :city => params[:rec_city],
                                    :delivery => params[:delivery], 
                                    :phone => params[:rec_phone], 
                                    :address => params[:rec_address], 
                                    :postal_code => params[:rec_postal_code], 
                                    :state_id => params[:rec_state_id],
                                    :country_id => 1228)
    end

    if @user_subscription.update_attributes(:box_id => params[:subscription_box_id].to_i, :package_id => @package.id, :total => @user_subscription.get_total)
      redirect_to @user_subscription
    end
  end
  
  def update
    @user_subscription = current_user.user_subscriptions.find(params[:id])
    card_token = params[:user_subscription][:stripe_card_token]
    customer_token = params[:user_subscription][:stripe_customer_token]

    if params[:subscription].present?
      @package = Package.find(params[:package])
      @subscriptions = params[:subscription]
      @total_boxes = @subscriptions.values.map {|s| s.to_f }.inject(:+)
      @total_price =  @total_boxes * @package.price

      if session[:subscription_id].present?
        @user_subscription = current_user.user_subscriptions.find(session[:subscription_id])
        @user_subscription.update_attributes(:total => @user_subscription.get_total)
      else
        @user_subscription = current_user.user_subscriptions.find(params[:id])
        session[:subscription_id] = @user_subscription.id.to_s
        @user_subscription.update_attributes(:box_id => params[:subscription_box_id].to_i, 
                                             :package_id => @package.id, 
                                             :total => @total_price.to_i)
      end

      if params[:rec_email] && params[:rec_address]
        @gifted_rec = @user_subscription.recipient_detail
        @gifted_rec.update_attributes(:name => params[:rec_name], 
                                      :message => params[:rec_message],
                                      :email => params[:rec_email], 
                                      :city => params[:rec_city],
                                      :delivery => params[:delivery], 
                                      :phone => params[:rec_phone], 
                                      :address => params[:rec_address], 
                                      :postal_code => params[:rec_postal_code], 
                                      :state_id => params[:rec_state_id],
                                      :country_id => 1228)
      end

      @user_subscription.line_items.destroy_all
      @subscriptions.each do |li|
        @line_item = @user_subscription.line_items.build
        if params[:gift] == "true"
          @line_item.update_attributes(:gift => true, :agegroup_id => li.first, 
                                       :qty => li.last, :package_id => @package.id, 
                                       :price => li.last.to_i * @package.price)
        else
          @line_item.update_attributes(:agegroup_id => li.first, 
                                       :qty => li.last, :package_id => @package.id, 
                                       :price => li.last.to_i * @package.price)
        end
      end
    end
    if params[:user_subscription][:subscription_id].present?
      if params[:ship_same_addr] == "on"
        @user_subscription.update_attributes(:billing_address => @user_subscription.shipping_address, 
                                             :zip_code => params[:zip_code], 
                                             :contact_no => params[:contact_no])
      else
        @user_subscription.update_attributes(:billing_address => params[:billing_address], 
                                             :zip_code => params[:zip_code],
                                             :contact_no => params[:contact_no])
      end
       if @user_subscription.update_attributes(:initial_customer_token => customer_token, :card_token => card_token)
        redirect_to user_subscription_final_verification_path(@user_subscription)
      else
        raise "false"
      end
    end
  end
  
  def continue_checkout
    @user_subscription = current_user.user_subscriptions.find(params[:id]) 
    if params[:address][:changed_address].present? 
      @user_subscription.update_attribute(:shipping_address, params[:address][:changed_address] || '')
      if params[:address][:promo_code].present?
        if @user_subscription.check_for_coupon_and_update_total(params[:address][:promo_code])
          flash[:notice] = "Coupon Redeemed Successfully. Now You have to pay only #{@user_subscription.total}"
        else
          flash[:error] = "Coupon Code Invalid!"
        end
      end
    end

    unless params[:address][:changed_address_gift].blank?
      @user_subscription.recipient_detail.update_attribute(:final_address, params[:address][:changed_address_gift])
      if params[:address][:promo_code_gift].present?
        if @user_subscription.check_for_coupon_and_update_total(params[:address][:promo_code_gift])
          flash[:notice] = "Coupon Redeemed Successfully. Now You have to pay only #{@user_subscription.total}"
        else
          flash[:error] = "Coupon Code Invalid!"
        end
      end
    end
    redirect_to :controller=>"user_subscriptions", :action => "new", :id => @user_subscription.id
  end
  
  def final_verification
    @user_subscription = current_user.user_subscriptions.find(params[:user_subscription_id])
    @card_token =  @user_subscription.card_token
    @initial_customer_token = @user_subscription.initial_customer_token
    @stripetoken = Stripe::Token.retrieve(@card_token)
    if params[:my_id].to_i == @user_subscription.id
      if @user_subscription.save_with_payment(@initial_customer_token) && @user_subscription.charge_card
        session[:subscription_id] = nil
        Mailer.order_receipt(@user_subscription, @stripetoken).deliver
        redirect_to order_receipt_path(@user_subscription), :notice => "Your order has been placed successfully."
      else
        if @user_subscription.errors.first.present?
          redirect_to :controller=>"user_subscriptions", :action => "new", :id => @user_subscription.id, :notice => "#{@user_subscription.errors.first.last}" 
        else
          redirect_to :controller=>"user_subscriptions", :action => "new", :id => @user_subscription.id, :notice => "Error occured!" 
        end
      end
    end
  end
end
