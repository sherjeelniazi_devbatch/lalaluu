class BlogsController < ApplicationController

	before_filter :get_categories

	def get_categories
		@categories = Category.where(:show_in_blog => true)
	end

	def index
		category = Category.find_by_slug('blog')
		@posts = Page.includes(author: [:profile]).where(category_id: category.id)
	end

	def show
		@page = Page.find_by_slug(params[:id])	
	end

	def categories
		@category = Category.find_by_slug(params[:slug])
		@category ? @posts = @category.pages : @posts =  nil
		render :index
	end

  private

	def use_https?
	  false
	end
end
