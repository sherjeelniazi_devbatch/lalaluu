class KidboxesController < ApplicationController
	before_filter :authenticate_user!, :except => [:index, :calculate_price]
	
	def index
		@kidbox = Box.current.first || Box.last
	end
	
	def subscribe
	    @kidbox = Box.find_by_id(params[:box_id])
	    @agegroup = @kidbox.agegroups
	    @states = State.where(:country_id => 1228).order(:name).includes(:country).inject({}) do |options, state|
	    	(options[state.country.name] ||= []) << [state.name, state.id] 
	    	options
	    end
	    @subscriptions = Subscription.all
  	end
  
    def post_subscribe
        redirect_to new_user_subscription_path(:plan_id => params[:subscription][:sub_id])
    end

    def calculate_price
    	@current_box = Box.current.first
    	@package = Package.find(params[:package_id])
    	@subscriptions = {}
    	params.each do |param|
		    if param[0].include?("subscription")  
		    	@subscriptions[param[0].scan(/\d/).first] = param[1]
		    end  
		end
		@total_boxes = @subscriptions.values.map {|s| s.to_f }.inject(:+)
    	@total_price =  @total_boxes * @package.price
    	respond_to do |format|
			format.js
		end
    end
end
