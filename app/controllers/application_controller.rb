class ApplicationController < ActionController::Base
  protect_from_forgery
  helper_method :namespace?, :permitted_params, :registering_user, :current_user_with_includes,:user_with_messages, :url_includes?
  before_filter :load_settings, :get_current_box, :https_redirect

  rescue_from CanCan::AccessDenied do |exception|
    flash[:error] = exception.message
    redirect_to (namespace?('admin') ? admin_users_path : root_url)
  end

  # def authenticate_user!
  #   redirect_to new_user_session_path, alert: 'You must be logged in.'
  # end


private #----------------------------------------


  def https_redirect
    if ENV["ENABLE_HTTPS"] == "yes"
      if request.ssl? && !use_https? || !request.ssl? && use_https?
        protocol = request.ssl? ? "http" : "https"
        flash.keep
        redirect_to protocol: "#{protocol}://", status: :moved_permanently
      end
    end
  end

  def use_https?
    true # Override in other controllers
  end
  
  def namespace?(str)
    params[:controller].split('/').first == str
  end

  def url_includes?(str)
    request.fullpath.split('/').include?(str)
  end

  def load_settings
  	@settings ||= Setting.first
  end

#####################################################################
  def user_from_session
    if session['warden.user.user.key']
      @user_from_session ||= session['warden.user.user.key'][1][0]
    else
      redirect_to root_url
    end
  end

  def current_user_with_includes
    @current_user_with_includes ||= User.includes(friends: [:profile]).find(user_from_session)
  end

#-----------------------------------------------------------------------------
  def user_with_messages
    @user_with_messages ||= User.includes(sent_messages:[recipients: [:profile]] ,recieved_messages: [sender: [:profile]]).find(user_from_session)
  end

  def user_with_sent_messages
    @user_with_messages ||= User.includes(sent_messages:[sender: [:profile],recipients: [:profile]]).find(user_from_session)
  end

  def user_with_recieved_messages
    @user_with_messages ||= User.includes(recieved_messages: [sender: [:profile]]).find(user_from_session)
  end
#-----------------------------------------------------------------------------  
#####################################################################

  def permitted_params
    @permitted_params ||= PermittedParams.new(params, current_user)
  end
  
  def registering_user
    @registering_user ||= User.find(session[:registering_user])
  end

  def get_current_user
    @user_current ||= current_user.present? ? current_user : registering_user
  end

  def get_current_box
    @kidbox ||= Box.current.first  
  end

  def create_favourites
    @user.build_favourite if @user.favourite.blank?
  end
  
end
