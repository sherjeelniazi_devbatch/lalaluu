class ProfilesController < ApplicationController

	def update
	    @profile = Profile.find(params[:id])
	    unless params[:profile][:avatar].present?
	    	authorize! :update, @profile
	    end
	    respond_to do |format|
	      if @profile.update_attribute(:avatar, params[:profile][:avatar])
	        format.html {redirect_to "/users/edit", notice: 'Profile was successfully updated.' }
	        format.json {render json: @profile}
	      end
	    end
  end
end
