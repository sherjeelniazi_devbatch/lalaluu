class LineItemsController < ApplicationController

	skip_before_filter :verify_authenticity_token

	def destroy
		@line_item = LineItem.find(params[:id])
		@user_subscription = @line_item.user_subscription
		@line_item.destroy
	    redirect_to user_subscription_url(@user_subscription)
	end
end
