class NewsLettersController < ApplicationController

  def new
    @letter = NewsLetter.new
    manipulate if current_user
  end

  def create
    @letter = NewsLetter.new(permitted_params.for_news_letters)
    if @letter.valid? && send_subscription_with_gibbon(@letter)
      redirect_to root_url, notice: 'A confirmation E-Mail has been sent. Please confirm to successfully subscrib.'
    else
      render 'new'
    end
  end

private #---------------------------------------------------------------
  def send_subscription_with_gibbon(letter)
    gb = Gibbon.new
    
    gb.list_subscribe({
      id: '9c9be000fd', 
      email_address: letter.email, 
      merge_vars: {
        FNAME: letter.first_name, 
        LNAME: letter.last_name
      }
    })
    rescue Gibbon::MailChimpError => e
     flash.now[:error] = e.message.gsub(/MailChimp API Error:/,'').gsub(/\(.+\)/,'!')
     false
  end

  def manipulate
    @letter.email = current_user.email
    @letter.first_name = current_user.profile.first_name
    @letter.last_name = current_user.profile.last_name
  end
end