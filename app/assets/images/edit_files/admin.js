(function() {

  jQuery(document).ready(function() {
    $('.pagination').addClass('alternate');
    $('.datepicker').datepicker({
      format: "yyyy-mm-dd"
    });
    if ($('.active').parent().is("ul") && !$('.active').parent().is(":visible")) {
      return $('.active').parent().show().parent().addClass('open');
    }
  });

}).call(this);
