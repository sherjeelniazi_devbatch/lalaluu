(function() {

  $(function() {
    return jQuery('.datepicker').datepicker({
      format: "yyyy-mm-dd"
    });
  });

}).call(this);
