$(document).ready -> 

  $("#rec_phone").mask("999-999-9999")
  pattern_email = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
  pattern_phone = /^[1-9]\d{2}-\d{3}-\d{4}$/
  
  
  $("#flipbox").hover(->
    $(this).children('img').attr('src', '/assets/special_offer_2.png')
  , ->
    $(this).children('img').attr('src', '/assets/special_offer.png')
  )


  validateEmail = (str) ->
    str.match pattern_email

  validatePhone = (str) ->
    str.match pattern_phone

  $("#subscription_select").submit ->
    $("#subscription_select input, textarea").each ->
      if $(this).val() == ''
        $(this).focus()
        $(this).addClass('inputError')
        false
      else
        $(this).removeClass('inputError')
    if validatePhone($("#rec_phone").val()) == null
      $("#rec_phone").focus()
      $("#rec_email").removeClass('inputError')
      $("#rec_phone").addClass('inputError')
      false
    else if validateEmail($("#rec_email").val()) == null
      $("#rec_email").focus()
      $("#rec_email").addClass('inputError')
      false 

  area = $('#subscription_select')
  firstRow  = area.find('.optionrow2:first')
  secondRow = firstRow.next()
  
  firstRow.hide()
  secondRow.hide()

  area.find('select').change ->
    if $(this).val() > 0 then showContent() else hideContent()

  $('.regular-radio').change ->
    showTotal()

  showContent = ->
    firstRow.show()
    showTotal()
  
  hideContent = ->
    firstRow.hide()
    secondRow.hide()

  showTotal = ->
    unless secondRow.is(':visible')
      if firstRow.find(':checked').length > 0 then secondRow.show() else secondRow.hide()

  $("#partner_brands").fancybox
    'hideOnContentClick': true
    'titlePosition'   : 'inside'

  $("#past_kidboxes").fancybox
    'hideOnContentClick': true
    'titlePosition'   : 'inside'

  #auto popilate the city
  $("#rec_postal_code").blur ->
    city = $("#rec_city")
    if $("#rec_postal_code").val()
      $.getJSON "http://www.geonames.org/postalCodeLookupJSON?&country=US&callback=?",
        postalcode: @value
      , (response) ->
        city.val response.postalcodes[0].placeName if not city.val() and response and response.postalcodes.length and response.postalcodes[0].placeName


  # ---------------------------------------------------------------------- 

 
