# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ -> 
  $("#subscription_select select").change ->
    get_total()
  $("input:radio[name=package]").click ->
    get_total()
  get_total = ->
    get_string = ""
    $("select").each (index) ->
      get_string += $(this).attr("id") + "=" + $(this).val() + "&"
    
    subscription = $("input:radio[name=package]:checked").val()
    
    $.ajax(
      url: "/calculate_price?" + get_string + "package_id=" + subscription 
      context: document.body
    )

jQuery ->
  Stripe.setPublishableKey($('meta[name="stripe-key"]').attr('content'))
  user_subscription.setupForm()
user_subscription =
  setupForm: ->
    $('#stripe_subscription').submit -> 
      $('input[type=submit]').attr('disabled', true)
      $('input[type=submit]').parent().append('<img id ="checkout-loader" src="/assets/checkout-loader.gif" />')
      if $('#card_number').length
        user_subscription.processCard() #for card token
        user_subscription.processCard() #for customer token
        false
      else
        true

  processCard: ->
    card =
      name: $('#cardholder_name').val()
      number: $('#card_number').val()
      cvc: $('#card_code').val()
      expMonth: $('#card_month').val()
      expYear: $('#card_year').val()

    Stripe.createToken(card, user_subscription.handleStripeResponse)
  
  handleStripeResponse: (status, response) ->
    if status == 200
      console.log(response)
      if($('#user_subscription_stripe_card_token').val())
        $('#user_subscription_stripe_customer_token').val(response.id)
        $('#stripe_subscription')[0].submit()
      else
        $('#user_subscription_stripe_card_token').val(response.id)
    else
      $('#checkout-loader').remove()
      $('#stripe_error').text(response.error.message)
      $('input[type=submit]').attr('disabled', false)
      $('html, body').animate({scrollTop:0}, 'slow')
      $('#cardholder_name').focus()