jQuery(document).ready ->

	$("#user_profile_attributes_phone_cell").mask("999-999-9999")
	$("#user_profile_attributes_state_id").val('32') #New york selected

	$("#user_profile_attributes_postal_code").blur ->
		city = $("#user_profile_attributes_city")
		if $("#user_profile_attributes_city").val() is ""
			$.getJSON "http://www.geonames.org/postalCodeLookupJSON?&country=US&callback=?",
				postalcode: @value
			, (response) ->
				city.val response.postalcodes[0].placeName if not city.val() and response and response.postalcodes.length and response.postalcodes[0].placeName
 
	if !$('.state_id select :selected').val()
	    $('.state_id').parent().hide()
	states = $('.state_id select').html()
	$('.country_id select').change ->
	    country = $('.country_id select :selected').text()
	    escaped_country = country.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
	    options = $(states).filter("optgroup[label='#{escaped_country}']").html()
	    if options
	        $('.state_id select').html(options)
	        $('.state_id').parent().show()
	    else
	        $('.state_id select').empty()
	        $('.state_id').parent().hide()

	$('input#ship_same_addr').click ->
		$('table#billing_address').toggle()

	$(".add_to_cart_link, .add_to_cart_link img").click ->
		$("form").submit()

	$(".blue_button.rounded#continue_to_payment").click -> 
		$("form").submit()

	$(".checkout_link, .checkout_link img").click ->
		$(".checkout_link submit").click()


	$('#user_favourite_attributes_kid_products_tokens').tokenInput '/kid_products.json'
	    theme: 'facebook'
	    prePopulate: $('#user_favourite_attributes_kid_products_tokens').data('load')

	$('#user_favourite_attributes_nyc_restaurants_tokens').tokenInput '/nyc_restaurants.json'
	    theme: 'facebook'
	    prePopulate: $('#user_favourite_attributes_nyc_restaurants_tokens').data('load')

	$('#user_favourite_attributes_nyc_playgrounds_tokens').tokenInput '/nyc_playgrounds.json'
	    theme: 'facebook'
	    prePopulate: $('#user_favourite_attributes_nyc_playgrounds_tokens').data('load')

	$('#user_favourite_attributes_nyc_spots_tokens').tokenInput '/nyc_spots.json'
	    theme: 'facebook'
	    prePopulate: $('#user_favourite_attributes_nyc_spots_tokens').data('load')


	$('#upload_avatar_link').click ->
	    $("#profile_avatar").click()

  	$('#edit_profile').fileupload
	    dataType: "json"
	    add: (e, data) ->
	    	types = /(\.|\/)(gif|jpe?g|png)$/i
	    	file = data.files[0]
	    	if types.test(file.type) || types.test(file.name)
	        	$('#user_avatar img').attr('src', '/assets/ajax-loader.gif')
	        	data.submit()
	      	else
	        	alert("#{file.name} is not a gif, jpeg, or png image file")
	    done: (e, data) ->
	    	profile = $.parseJSON(data.jqXHR.responseText)
	    	$('#user_avatar img').attr('src', profile.avatar.thumb.url)


	$('form').on 'click', '.remove_fields', (event) ->
		$(this).prev('input[type=hidden]').val('1')
		$(this).closest('fieldset').hide()
		event.preventDefault()

	$('form').on 'click', '.add_fields', (event) ->
		time = new Date().getTime()
		regexp = new RegExp($(this).data('id'), 'g')
		$(this).parent().before($(this).data('fields').replace(regexp, time))
		event.preventDefault()

	if $('#error_explanation').length == 0
		$('.profile').find('.page_title').parent().next().hide()
	
	# $('.profile').find('.page_title').click ->
	# 	$(this).parent().next().slideToggle()

	$('.delete_spouse').click ->
		$(this).prev('input[type=hidden]').val('1')
		$(this).fadeOut(500).parent().prevAll('.input').fadeOut(500)
		$(this).parent().before('Please hit Update to commit changes!')
		false

	# FACEBOOKK INVITATION ------------------------>
	# assume we are already logged in
	$('.sendInvitatioFacebook').click ->
		$.getJSON("/users/invitations/generate_token.json?api=facebook", (data) ->
			token = data.toString()
			FB.init
			  appId: "373709476047138"
			  xfbml: true
			  cookie: true

			FB.ui
			  method: "send"
			  name: "LaLaaLu"
			  # link: "http://www.facebook.com?token=#{token}"
			  link: "http://lalaalu.herokuapp.com/users/friendships/new?token=#{token}"
			  picture: "http://lalaalu.herokuapp.com/assets/logo.png"			
		).error(->
			alert "Unknown error occured! Please referesh the page and try again!"
		)
		false

	# OVERLAY LOGIC -----------------------------
	showOverlay = -> $('#overlay').fadeIn()
	hideOverlay = -> $('#overlay').fadeOut()

	mouse_is_inside = false  
	$('.overlay_inside').hover (->
		mouse_is_inside = true
	), ->
		mouse_is_inside = false

	$("body").click ->
		unless mouse_is_inside
			hideOverlay()
	
	$('.close_overlay').click -> 
		hideOverlay()

	# FQAs Page
	$('.faq .question').click ->
		$(this).toggleClass('blue').parent().find('.answer').slideToggle()
		$(this).find('span:first').toggleClass('minus')

	$('.optionrow1').click ->
		$(this).toggleClass('blue').parent().find('.details').slideToggle()
		$(this).find('span:first').toggleClass('minus')

