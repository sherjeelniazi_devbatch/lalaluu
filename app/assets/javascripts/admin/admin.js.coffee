jQuery(document).ready ->
	$('.pagination').addClass('alternate')
	$('.datepicker').datepicker format: "yyyy-mm-dd"

	if $('.active').parent().is("ul") && !$('.active').parent().is(":visible")
		$('.active').parent().show().parent().addClass('open')