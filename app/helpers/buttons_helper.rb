module ButtonsHelper
  
  def styled_button(type, url, options = {})
    iklass = 
    case type
      when 'new'    then 'icon-plus-sign'
      when 'create' then 'icon-plus-sign'
      when 'update' then 'icon-refresh'
      when 'edit'   then 'icon-pencil'
      when 'show'   then 'icon-eye-open'
      when 'delete' then 'icon-remove'
      when 'back'   then 'icon-arrow-left'
    end
    link_to_button(type, url, iklass.concat(' icon-white'), options)
  end

  def link_to_button(type, url,iklass,options = {})
    options.merge_keep_orignal!(
        class: 'btn btn-inverse btn-mini', 
        title: type.capitalize
    )
    if type == 'delete'
      button_to('X',url,options)
    else
      link_to( content_tag(:i,nil,class: iklass).concat(''), url, options)    
    end
  end
#--------------------------------------------------------------------------

  def styled_form_button(type)
  	 case type
	  	 when 'create' then create_button(type, :submit, 'icon-plus-sign')
	  	 when 'update' then create_button(type, :submit,'icon-refresh')
	  	 when 'reset'  then create_button(type, :reset, 'icon-repeat')	 	
  	 end
  end

  def create_button(name, action, iklass)
  	iklass.concat(' icon icon-white')
    content_tag(:button, type: action, class: 'btn btn-inverse') do 
    	content_tag(:i, nil, class: iklass).concat(' '+name.capitalize)
	  end
  end
#--------------------------------------------------------------------------

  def link_li(iklass, name, url='')
    klass = set_active(name)
    url = url.blank? ? send("admin_#{name}_path") : url

    content_tag(:li, class: klass ) do
      link_to content_tag(:i,nil,class: iklass).concat(content_tag(:span,name.split().map { |w| w.humanize }.join(' '))),
        url
    end 
  end

end