module ApplicationHelper
  include ButtonsHelper

  def get_salutation(i_am)
    i_am == "DAD" ? "Mr." : "Mrs."
  end

  def include_javascript(*args) 
    args.each do |jsfile|  
      content_for :javascript_includes, javascript_include_tag(jsfile)
    end
  end

  def nl2br(s)
     s.gsub(/\n/, '<br>').html_safe unless s.nil?
 end
 
  def include_stylesheet(*args)
  	args.each do |cssfile|
  		content_for :stylesheet_includes, stylesheet_link_tag(cssfile)
  	end
  end

  def user_address(user_profile)
    user_profile.street_address + ", \n" + user_profile.city + ", " + user_profile.state.name + ", " + user_profile.postal_code + ", \n" + user_profile.country.name rescue nil
  end

  def recipient_address(user_subscription)
    recipient = user_subscription.recipient_detail
    recipient.address + ", \n"  + recipient.city + ", " + recipient.state.name + ", " + recipient.postal_code + ", \n" + recipient.country.name rescue nil
  end
  

  #---------------------------------------------------------------->
  
  def is_home?
    request.url == root_url
  end
  
  def is_dashboard?
    request.params[:controller] == 'users/boxes' or request.params[:controller] == 'users/dashboard'
  end

  def is_blog?
    request.url.include?(blogs_path) || request.url.include?("categories/")
  end

  def is_current_page?(*kontroller)
    kontroller.include?(controller.controller_name)
  end

  def set_active(str)
    is_current_page?(str) ? 'active' : ''
  end

  def link_to_add_fields(name, f, association,partial='')
    new_object = f.object.send(association).klass.new
    id = new_object.object_id
    fields = f.simple_fields_for(association, new_object, child_index: id) do |builder|
      partial = association.to_s.singularize.concat("_fields") if partial.blank?
      render(partial, f: builder)
    end
    link_to(name, '#', class: "add_fields", data: {id: id, fields: fields.gsub("\n", "")})
  end
  
  def print_current_nav
    case controller.controller_name
      when 'sessions' then 'Sign In / Sign Up'
      when 'registrations' then 'profile'
      when 'pages'    then params[:slug]
      else; controller.controller_name
    end.humanize
  end
  
  def format_boolean(status)
    if status
      icon_tick
    else
      icon_cross
    end
  end
  
  def icon_tick(alt_text='Tick')
    build_image_tag("/assets/001_06.png", alt_text)
  end

  def icon_cross(alt_text='Cross')
    build_image_tag("/assets/001_05.png", alt_text)
  end
  
  def build_image_tag(image_file, alt_text)
    image_tag(image_file, :size =>"20x20", :alt => alt_text)
  end

  def user_img_or_default_img
    image_tag current_user.profile.try(:avatar).present? ? current_user_with_includes.profile.avatar_url(:thumb).to_s : 'user-icon.png'
  end

  def profile_image(user=current_user, options={})
    image = user.profile.try(:avatar).present? ? user.profile.avatar_url(:thumb).to_s : 'default_user.png'
    image_tag(image,options)
  end

  def msg_user_name(id)
    usr = Profile.find_by_user_id(id)
    @name = usr.first_name + "  " + usr.last_name
  end

  def user_msgs
    @countmsg = Message.where("opened = ? AND deleted = ? AND received_messageable_id = ? ", false, false, current_user.id).count
  end

  def read_background(read)
    if read == false
      @backgd = "background-color:#d9d0e0;"
    else
      @backgd = "background-color:white;"
    end


  end

end # CLASS ENDS 

# ADD METHODS TO CLASSES ---------------------------------------->
class Hash
  def merge_keep_orignal!(options={})
    merge!(options) {|key, first, second| first + " " + second }
  end
end
