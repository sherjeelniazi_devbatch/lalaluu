module UserSubscriptionsHelper

	def get_box_path(box, gift = false)
		if gift == true
			gift_str = "&gift=gift"
		else
			gift_str = ""
		end
		subscribe_kidboxes_path + "?box_id=" + box + gift_str
	end
end
