class Mailer < ActionMailer::Base
	include ApplicationHelper
    helper :application

	default from: "info@#{ENV['APP_HOST']}"
	@host = "http://www.lalaalu.com"
	
	def invite_friends(user, friend)
		@user 	= user
		@friend = friend

    	mail to: @friend.friend_email, subject: 'Invitation - LaLaaLu'
	end

	def order_receipt(order, billing)
		@order = order
		@billing = billing
    	@settings = Setting.first
		mail to: @order.user.email, subject: 'Order Receipt - LaLaaLu', bcc: [User.where(:role => 'superadmin').map {|user| user.email }]
	end

	def send_it_to_my_friends(sender, recipient, message)
		@sender = sender
		@message = message

		mail to: recipient.email, subject: "LaLaaLu -- #{message.subject}"
	end

	def test_email(user)
		@user = user
		@settings = Setting.first
		mail to: user.email, subject: "LaLaaLu -- test"
	end
end