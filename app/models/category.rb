class Category < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: [:slugged, :history]
  attr_accessible :name, :subcategory_id, :slug, :show_in_blog


  has_many :pages
end
