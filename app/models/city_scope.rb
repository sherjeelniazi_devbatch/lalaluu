class CityScope < ActiveRecord::Base
  attr_accessible :image_url, :redirect_url ,:upload
  mount_uploader :upload, CityScopeUploader
    
end
