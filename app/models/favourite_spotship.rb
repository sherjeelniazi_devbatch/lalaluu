class FavouriteSpotship < ActiveRecord::Base
  attr_accessible :favourite_id, :nyc_spot_id
  belongs_to :favourite
  belongs_to :nyc_spot
end
