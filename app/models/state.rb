class State < ActiveRecord::Base
  attr_accessible :country_id, :name
  belongs_to :country
  has_many :profiles
  has_many :recipient_details
end
