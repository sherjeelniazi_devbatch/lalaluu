class FavouriteRestaurantship < ActiveRecord::Base
  attr_accessible :favourite_id, :nyc_restaurant_id
  belongs_to :favourite
  belongs_to :nyc_restaurant
end
