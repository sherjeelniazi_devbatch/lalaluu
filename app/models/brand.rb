class Brand < ActiveRecord::Base
  has_many :products
  # attr_accessible :created_by, :description, :name, :brand_image, :remote_brand_image_url
  mount_uploader :brand_image, BrandImageUploader
end
