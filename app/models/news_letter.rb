class NewsLetter
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming
  
  attr_accessor :first_name, :last_name, :email

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :email, presence: true, email: true

  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end
  
  def persisted?
    false
  end

end