class PermittedParams < Struct.new(:params, :user)

# --- USER --------------------------------------------->
	def for_user
	    if user && user.role?(:superadmin)
	      params[:user].permit! # ALLOW ALL
	    else
	      params[:user].permit(:username, :email, :password, :password_confirmation, :remember_me, :profile_attributes, :spouse_attributes, :children_attributes,:favourite_attributes, :_destroy)
	    end
	end
	
	def for_user_subscription
	  params[:user_subscription].permit!
    end

	def for_profile
		params.require(:profile).permit(:avatar, :title, :first_name, :last_name, :street_address, :city, :state, :postal_code, :phone_cell, :phone_home, :i_am,:display_name, :age, :hobbies, :birthday )
	end

	def for_line_items
		params.require(:line_item).permit!
	end

	def for_spouse
		params.require(:spouse).permit(:email, :first_name, :last_name, :birthday)
	end

	def for_invitation
		params.require(:invitation).permit(:friend_email, :message)
	end

	def for_message
		params.require(:message).permit(:subject,:content)
	end

	def for_news_letters
		params.require(:news_letter).permit!
	end
# --- DEVISE --------------------------------------------->

	def for_devise_registration
		params[:user].permit(:username, :email, :password, :password_confirmation, :remember_me, :current_password, :profile_attributes, :children_attributes, :spouse_attributes,:favourite_attributes,:_destroy)
	end

	def for_devise_password
		params.require(:user).permit(:email, :password, :password_confirmation, :reset_password_token)
	end

# --- DEVISE --------------------------------------------->

	%w(setting package family_curator faq feed coupon brand product box order page subscription user_subscription agegroup category).each do |model|
		define_method("for_#{model}") do
			if user && user.admin?
				params.require(model.to_sym).permit!
			end
		end
	end
end