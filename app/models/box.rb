class Box < ActiveRecord::Base
  has_and_belongs_to_many :agegroups
  has_and_belongs_to_many :products
  has_many :user_subscriptions
  has_one :coupon
  # attr_accessible :end_date, :name, :price, :start_date, :product_ids, :box_image, :description, :age_group, :active, :offer
  mount_uploader :box_image, BoxImageUploader

  scope :current, lambda { where("start_date >= ? AND end_date <= ?", Date.today.at_beginning_of_month, Date.today.at_end_of_month) }
end
