class FavouriteKidProduct < ActiveRecord::Base
  	attr_accessible :name
  	has_many :favourites, :through => :kid_productship
	has_many :kid_productship
	

	def self.tokens(query)
	  kid_product = where("name like ?", "%#{query}%")
	  if kid_product.empty?
	    [{id: "<<<#{query}>>>", name: "New: \"#{query}\""}]
	  else
	    kid_product
	  end
	end

	def self.ids_from_tokens(tokens)
	  	tokens.gsub!(/<<<(.+?)>>>/) { create!(name: $1).id } 
	  	tokens.split(',')
	end
end
