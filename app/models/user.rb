class User < ActiveRecord::Base
  # :omniauthable, :encryptable

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :token_authenticatable, :lockable, :timeoutable #, :confirmable
  
  has_one  :profile,  dependent: :destroy, validate: true
  has_one  :spouse,   dependent: :destroy, validate: true
  has_one  :favourite, dependent: :destroy, validate: true
  has_many :children, dependent: :destroy, validate: true
  
  has_many :invitations, dependent: :destroy, validate: true
  belongs_to :invitation
  
  has_many :friendships
  has_many :friends, through: :friendships, conditions: ["friendships.status = 'approved'"]
  has_many :pending_friends, through: :friendships, conditions: ["friendships.status = 'pending'"], source: :friend

  has_many :inverse_friendships, class_name: "Friendship", foreign_key: "friend_id"
  has_many :inverse_friends, through: :inverse_friendships, source: :user, conditions: ["friendships.status = 'approved'"]
  has_many :inverse_pending_friends, through: :inverse_friendships, source: :user, conditions: ["friendships.status = 'pending'"]

  has_many :messagings, foreign_key: :sender_id
  has_many :sent_messages, through: :messagings, source: :message, conditions: ["messagings.placed_as != 'trash'"]
  has_many :inverse_messagings, :class_name => "Messaging", :foreign_key => "recipient_id"
  has_many :recieved_messages, through: :inverse_messagings, :source => :message, conditions: ["messagings.placed_as is null"]
  has_many :draft_messages, through: :messagings , source: :message, conditions: ["messagings.placed_as = 'draft'"]

  has_many :boxes, :through => :user_subscriptions, dependent: :destroy
  has_many :agegroups, :through => :user_subscriptions, dependent: :destroy
  has_many :user_subscriptions#, :through => :user_subscriptions, dependent: :destroy
  has_many :pages, foreign_key: :author_id

  accepts_nested_attributes_for :profile
  accepts_nested_attributes_for :spouse, :allow_destroy => true, :reject_if => lambda { |a| a[:email].blank? }
  accepts_nested_attributes_for :favourite
  accepts_nested_attributes_for :children, :reject_if => lambda { |a| a[:name].blank? }, :allow_destroy => true

  # CALLBACKS --------------

  scope :get_spouse_and_children, includes(:profile,{spouse: [:profile]},{children: [:profile]})

  has_mailbox

  def create_child_profile 
    children.build.build_profile
  end
  
  def confirmed?
    true
  end

  def role?(role)
    self.role == role.to_s
  end
  
  def admin?
    self.role?(:admin) || self.role?(:superadmin)
  end
end
