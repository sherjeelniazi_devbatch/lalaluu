class Page < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: [:slugged, :history]
  # attr_accessible :body, :category_id, :slug, :title, :show_title, :unpublished
  
  validates_uniqueness_of :slug
  validates_presence_of :title
  # attr_accessible :body, :category_id, :slug, :title, :hidden

  belongs_to :category
  belongs_to :author, class_name: 'User'
end
