class Product < ActiveRecord::Base
  belongs_to :brand
  has_and_belongs_to_many :boxes
  # attr_accessible :name, :box_ids, :brand_id, :description, :product_image, :remote_brand_image_url
  mount_uploader :product_image, ProductImageUploader
end
