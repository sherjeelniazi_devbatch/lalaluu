class RecipientDetail < ActiveRecord::Base
  belongs_to :user_subscription
  belongs_to :country
  belongs_to :state
  attr_accessible :address, :email, :message, :delivery, :city, :name, :phone, :user_subscription_id, :state_id, :country_id, :postal_code

  # validates_presence_of :address, :email, :name, :phone
end
