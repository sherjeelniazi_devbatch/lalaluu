class Favourite < ActiveRecord::Base
	attr_accessible :kid_products_tokens, :nyc_restaurants_tokens, :nyc_playgrounds_tokens, :nyc_spots_tokens
	attr_reader :kid_products_tokens, :nyc_restaurants_tokens,:nyc_playgrounds_tokens,:nyc_spots_tokens
	belongs_to :user

	has_many :favourite_kid_products, :through => :kid_productship
	has_many :kid_productship

	has_many :nyc_restaurants, :through => :favourite_restaurantship
	has_many :favourite_restaurantship

	has_many :nyc_playgrounds, :through => :favourite_playgroundship
	has_many :favourite_playgroundship

	has_many :nyc_spots, :through => :favourite_spotship
	has_many :favourite_spotship

	
	def kid_products_tokens=(tokens)
		self.favourite_kid_product_ids = FavouriteKidProduct.ids_from_tokens(tokens)
	end

	def nyc_restaurants_tokens=(tokens)
		self.nyc_restaurant_ids = NycRestaurant.ids_from_tokens(tokens)
	end

	def nyc_playgrounds_tokens=(tokens)
		self.nyc_playground_ids = NycPlayground.ids_from_tokens(tokens)
	end

	def nyc_spots_tokens=(tokens)
		self.nyc_spot_ids = NycSpot.ids_from_tokens(tokens)
	end
end