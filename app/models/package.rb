class Package < ActiveRecord::Base
  attr_accessible :name, :price, :trial_days, :gift_it
  has_many :line_items
  has_many :user_subscriprtions
  #scope :giftpackage, where(:id != 9)
  
  def self.giftpackage
    self.where(:gift_it => true)
  end
end
