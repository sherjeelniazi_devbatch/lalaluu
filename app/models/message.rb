class Message < ActiveRecord::Base
  has_mailbox
  has_many :messagings
  has_many :recipients, through: :messagings, source: :recipient

  has_one :message_copies

  has_one :inverse_messagings, :class_name => "Messaging"
  has_one :sender, through: :inverse_messagings, source: :sender

  validates :subject, presence: true
  validates :content, presence: true

  after_create :set_message_status

  def set_message_status
  	self.messagings.where('sender_id is null').each do |m|
  		m.status = 'unread'
  		m.save
  	end
  end

  def set_sender_and_recipient(sender,recipient)
    self.sender = sender
    self.recipients << recipient
  end
  
end