class FavouritePlaygroundship < ActiveRecord::Base
  attr_accessible :favourite_id, :nyc_playground_id
  belongs_to :favourite
  belongs_to :nyc_playground
end
