class Country < ActiveRecord::Base
  attr_accessible :name
  has_many :states
  has_many :profiles
  has_many :recipient_details
end
