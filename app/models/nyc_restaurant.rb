class NycRestaurant < ActiveRecord::Base
  attr_accessible :name
  has_many :favourites, :through => :favourite_restaurantship
  has_many :favourite_restarantship
	

	def self.tokens(query)
	  favourite_restarant = where("name like ?", "%#{query}%")
	  if favourite_restarant.empty?
	    [{id: "<<<#{query}>>>", name: "New: \"#{query}\""}]
	  else
	    favourite_restarant
	  end
	end

	def self.ids_from_tokens(tokens)
	  	tokens.gsub!(/<<<(.+?)>>>/) { create!(name: $1).id } 
	  	tokens.split(',')
	end
end
