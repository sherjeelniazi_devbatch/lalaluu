class NycPlayground < ActiveRecord::Base
  attr_accessible :name
  has_many :favourites, :through => :favourite_playgroundship
  has_many :favourite_playgroundship
	

	def self.tokens(query)
	  favourite_playground = where("name like ?", "%#{query}%")
	  if favourite_playground.empty?
	    [{id: "<<<#{query}>>>", name: "New: \"#{query}\""}]
	  else
	    favourite_playground
	  end
	end

	def self.ids_from_tokens(tokens)
	  	tokens.gsub!(/<<<(.+?)>>>/) { create!(name: $1).id } 
	  	tokens.split(',')
	end
end
