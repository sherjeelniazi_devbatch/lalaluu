class Ability
  include CanCan::Ability
  
  def initialize(user)
  	user ||= User.new # guest
  	
  	if user.role? :superadmin
      can :manage, :all
    elsif user.role? :admin
      can :read, :all
      can :manage, Setting
      can :manage, Faq
      can :manage, Feed
      can :manage, Coupon
      can :manage, Product
      can :manage, Brand
      can :manage, Box
      can :manage, Page
      can :manage, Subscription
      can :manage, Agegroup
    else
      can :manage, LineItem
    end
  end

end