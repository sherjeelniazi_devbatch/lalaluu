class Agegroup < ActiveRecord::Base
  # attr_accessible :title, :body
  has_and_belongs_to_many :boxes
  belongs_to :boxes
  has_many :line_items

end
