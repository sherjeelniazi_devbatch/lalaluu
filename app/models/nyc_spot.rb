class NycSpot < ActiveRecord::Base
  attr_accessible :name
  has_many :favourites, :through => :favourite_spotship
  has_many :favourite_spotship
	

	def self.tokens(query)
	  favourite_spotship = where("name like ?", "%#{query}%")
	  if favourite_spotship.empty?
	    [{id: "<<<#{query}>>>", name: "New: \"#{query}\""}]
	  else
	    favourite_spotship
	  end
	end

	def self.ids_from_tokens(tokens)
	  	tokens.gsub!(/<<<(.+?)>>>/) { create!(name: $1).id } 
	  	tokens.split(',')
	end
end
