class Setting < ActiveRecord::Base

  attr_accessible :facebook_page, :twitter_page, :home_page, :pinterest_page, :partner_brands
  validates :facebook_page, presence: true

end 	