class KidProductship < ActiveRecord::Base
  attr_accessible :favourite_id, :favourite_kid_product_id
  belongs_to :favourite
  belongs_to :favourite_kid_product
end
