class Invitation < ActiveRecord::Base
	belongs_to :sender, class_name: 'User', foreign_key: :user_id
	has_one :recipient, class_name: 'User'

	validates :friend_email, presence: true, email: true

	attr_accessor :valid_objs, :invalid_objs

	def initialize(*args)
		super
		self.valid_objs = []
		self.invalid_objs = []
	end

	def prepare_mails
		friend_email.split(',').map &:strip
	end

	def verify_data
		prepare_mails.each do |mail|
			token = generate_token(mail)
			data 	= { friend_email: mail, token: token, message: message, sent_at: Time.now}
			obj 	= sender.invitations.build(data)
				
			obj.valid? ? (valid_objs << obj) : (invalid_objs << obj)
		end
	end

	def generate_token(mail)
		Digest::SHA1.hexdigest(Time.now.to_s + mail +'Winter is here.')
	end

	def save_for_api(user, api)
		self.friend_email = (0...8).map{65.+(rand(26)).chr}.join.concat("@#{api}.com")
		self.token = generate_token(self.friend_email)
		self.user_id = user.id
		self.sent_at = Time.now
		self.save!
	end
end