class Friendship < ActiveRecord::Base
	belongs_to :user
	belongs_to :friend, class_name: 'User'

  before_create { self.status = 'pending' }

  has_mailbox

  def approve
    self.status = 'approved'
    self.confirmed_at = Time.now
    self.save
  end
end