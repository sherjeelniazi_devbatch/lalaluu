class UserSubscription < ActiveRecord::Base
  attr_accessor :stripe_card_token


  attr_accessible :user_id, :total, :box_id, :event_token, :stripe_customer_token, :subscription_id, :package_id, :billing_address, :zip_code, :contact_no, :paymentstatus, :initial_customer_token, :card_token

  has_many :line_items, :dependent => :destroy
  has_one :recipient_detail
  belongs_to :user
  belongs_to :package
  belongs_to :box
  
  # Method For Stripe.
  def save_with_payment(customer_token)
      if self.line_items.count > 0
        trial_ends = (Time.now+package.trial_days.days).utc.to_i
        customer = Stripe::Customer.create(plan: 5, card: customer_token, trial_end: trial_ends, email: user.email)
      else
        customer = Stripe::Customer.create(plan: 5, card: customer_token)
      end
        self.stripe_customer_token = customer.id
      save!
      rescue Stripe::InvalidRequestError => e
        logger.error "Stripe error while creating customer: #{e.message}" 
        errors.add :stripe, "There was a problem with your credit card."
        false
      rescue Stripe::CardError => e
        logger.error "Stripe error while creating charging the client: #{e.message}"
        errors.add :stripe, "#{e.message}"
        false
  end

  def self.to_csv(options = {})

    CSV.generate(options) do |csv|
      columns = ['User','Billing Address', 'Recipient Message', 'Recipient Name', 'Recipient Email', 'Recipient Phone', 'Recipient Address']
      csv << columns
      all.each do |order|
        email = order.user.present? ? order.user.email : ''
        billing_address = order.billing_address
        if order.recipient_detail.present?
          rec_message = order.recipient_detail.message
          rec_name = order.recipient_detail.name
          rec_email = order.recipient_detail.email
          rec_phone = order.recipient_detail.phone
          rec_final_address = order.recipient_detail.final_address
        end
        values = [email, billing_address, rec_message, rec_name, rec_email, rec_phone, rec_final_address]
        csv << values
      end
      csv
    end
  end

  def get_total
   @total = sub_total
   (@total + (@total * (8.875/100)))
  end

  def sub_total
    line_items.map(&:price).inject(&:+)
  end

  def charge_card
    charge = Stripe::Charge.create(
      :amount => (total * 100).to_i, # amount in cents, again
      :currency => "usd",
      :card => self.card_token,
      # :customer => stripe_customer_token,
      :description => user.email
    )
    self.paymentstatus = true
    self.event_token = charge.id
    save!
    rescue Stripe::CardError => e
        logger.error "Stripe error while creating charging the client: #{e.message}"
        errors.add :stripe, "#{e.message}"
        false
    rescue Stripe::InvalidRequestError => e
        logger.error "Stripe error while creating charging the client: #{e.message}"
        errors.add :stripe, "There was a problem with your credit card."
        false
  end

  def stripe_charge
    Stripe::Charge.retrieve(self.event_token)
  end

# Method for coupon validation. 
  def check_for_coupon_and_update_total(coupon_code)
    @coupon = self.box.coupon
    if (@coupon.name == coupon_code) && (@coupon.available)
      @discount = (@coupon.discount.to_f/100)*(self.total)
      @discounted_total = (self.total - @discount)
      self.discount = @discount
      self.total = @discounted_total
      save!
      true
    else
      false
    end
  end
  
end
