class FamilyCurator < ActiveRecord::Base
  attr_accessible :details, :kid_ages, :names, :picture_string, :summary
  mount_uploader :picture_string, CuratorUploader
end
