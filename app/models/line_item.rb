class LineItem < ActiveRecord::Base
  attr_accessible :agegroup_id, :gift_it, :package_id, :price, :qty, :user_subscription_id
# Was giving problems when encountered in a single line. So had to divide them into multiple ones.
  belongs_to :agegroup 
  belongs_to :package
  belongs_to :user_subscription
  
end
