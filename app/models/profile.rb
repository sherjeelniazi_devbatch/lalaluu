class Profile < ActiveRecord::Base
  mount_uploader :avatar, AvatarUploader
  
  belongs_to :user
  belongs_to :country
  belongs_to :state

  
  validates :first_name,   presence: true
  validates :display_name, presence: true
  validates :last_name,    presence: true
  validates :street_address, presence: true
  validates :postal_code,  presence: true
  #validates :birthday,     presence: true
  validates :i_am,         presence: true
  validates :city,         presence: true

  def address
    "#{street_address}, #{postal_code}" if street_address.present? || city.present? || postal_code.present?
  end

  def make_birthday
  	birthday.strftime('%B %d, %Y') if birthday.present?
  end

  def full_name
  	"#{title} #{first_name} #{last_name}"
  end
end
