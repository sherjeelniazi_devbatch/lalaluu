class EmailValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if value.length < 8 
      record.errors[attribute] << (options[:message] || "Too short Min 8 Characters")
    end
    
    if value.length > 50 
      record.errors[attribute] << (options[:message] || "Too long Max 50 Characters")
    end

    unless value =~ /^(|(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6})$/i
      record.errors[attribute] << (options[:message] || "Please Enter a valid Email")
    end
  end
end