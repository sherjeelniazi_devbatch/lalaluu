Lalaalu::Application.routes.draw do

  mount Ckeditor::Engine => "/ckeditor"

  mailboxes_for :users

  devise_for :users, path_names: { sign_in: "login", sign_out: "logout" }, 
             :controllers => { 
                :registrations => "users/registrations", 
                :passwords => "users/passwords",
                :sessions => "users/sessions"
              }
  match 'kid_products' => 'users/favourites#kid_products' 
  match 'nyc_restaurants' => 'users/favourites#nyc_restaurants' 
  match 'nyc_spots' => 'users/favourites#nyc_spots' 
  match 'nyc_playgrounds' => 'users/favourites#nyc_playgrounds'

  match 'save_avatar' => 'users/spouses#save_avatar'
  match 'pages/:slug' => 'pages#show', :as => :pages
  post '/tinymce_assets' => 'tinymce_assets#create'
  get '/admin' => 'admin/pages#index'
  get '/subscribe-kidboxes'=> 'kidboxes#subscribe', :as => 'subscribe_kidboxes'
  post '/subscribe-kidboxes'=> 'kidboxes#subscribe'
  get '/gift-kidboxes'=> 'kidboxes#subscribe_gift'
  post '/gift-kidboxes'=> 'kidboxes#subscribe_gift'
  match '/user-subscribe', :to =>'kidboxes#post_subscribe'

  match '/user_subscriptions/:id/continue_checkout', :to =>'user_subscriptions#continue_checkout'
  #match '/user_subscriptions/:id/final_verification', :to =>'user_subscriptions#final_verification'

  match '/user_subscriptions/:id/new', :to =>'user_subscriptions#new'

  match '/blogs/categories/:slug' => 'blogs#categories', :as => :blogs_category
  get '/calculate_price' => 'kidboxes#calculate_price'
  match '/users/invitations/generate_token' => 'users/invitations#generate_token'

  match '/users/dashboard' => 'users/dashboard#index'
  
  resources :home
  resources :line_items
  resources :news_letters
  resources :pages
  resources :blogs
  resources :kidboxes
  resources :order_receipts
  resources :user_subscriptions do
    get :final_verification  
  end

  resources :profiles
  resources :spouses
  resources :faqs
  resources :family_curators



  namespace :users do
    resources :spouses
    resources :children
    resources :favourites
    resources :invitations
    resources :friendships
    get 'compose_message/:id', :to => 'friendships#compose_message'
    post "messages/send_message" => "messages#send_message"
    #post :send_message
    get "messages/del_msg/:id" => "messages#del_msg"
    get "messages/back_inbox/:id" => "messages#back_inbox"
    get "messages/del_all_msgs" => "messages#del_all_msgs"
    resources :messages
    resources :boxes
  end

  namespace :admin do 
    resources :boxes
    resources :packages
    resources :categories
    resources :family_curators
    resources :brands
    resources :coupons
    resources :faqs
    resources :feeds
    resources :products
    resources :settings
    resources :orders
    match '/orders/:id/paymentstatus', :to => "orders#toggled_status"
    match '/blog_posts', :to => "pages#blogs", :as => 'blog_posts'
    resources :users
    resources :pages
    resources :subscriptions
    resources :agegroups
    resources :users
    resources :city_scopes
  end
  
  
  root :to => 'home#index'

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
