# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130116222752) do

  create_table "admin_settings", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "agegroups", :force => true do |t|
    t.string   "group_name"
    t.integer  "start_age"
    t.integer  "end_age"
    t.string   "format"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "agegroups_boxes", :id => false, :force => true do |t|
    t.integer "agegroup_id"
    t.integer "box_id"
  end

  create_table "attachments", :force => true do |t|
    t.string   "image"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "boxes", :force => true do |t|
    t.string   "name"
    t.float    "price"
    t.date     "start_date"
    t.date     "end_date"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "box_image"
    t.text     "description"
    t.string   "age_group"
    t.boolean  "active"
    t.boolean  "offer"
    t.integer  "agegroup_id"
  end

  create_table "boxes_products", :id => false, :force => true do |t|
    t.integer "box_id"
    t.integer "product_id"
  end

  create_table "brands", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "created_by"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "brand_image"
  end

  create_table "categories", :force => true do |t|
    t.string   "name"
    t.integer  "subcategory_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.string   "slug"
    t.boolean  "show_in_blog"
  end

  create_table "children", :force => true do |t|
    t.integer  "user_id",    :default => 0, :null => false
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
    t.string   "name"
    t.datetime "birthday"
  end

  add_index "children", ["user_id"], :name => "index_children_on_user_id"

  create_table "city_scopes", :force => true do |t|
    t.string   "image_url"
    t.string   "redirect_url"
    t.string   "upload_file_name"
    t.string   "upload_content_type"
    t.integer  "upload_file_size"
    t.datetime "upload_updated_at"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.string   "upload"
  end

  create_table "ckeditor_assets", :force => true do |t|
    t.string   "data_file_name",                  :null => false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    :limit => 30
    t.string   "type",              :limit => 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], :name => "idx_ckeditor_assetable"
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], :name => "idx_ckeditor_assetable_type"

  create_table "countries", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "coupons", :force => true do |t|
    t.string   "name"
    t.integer  "discount",   :default => 0
    t.boolean  "available",  :default => false
    t.integer  "box_id"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  add_index "coupons", ["box_id"], :name => "index_coupons_on_box_id"
  add_index "coupons", ["name"], :name => "index_coupons_on_name"

  create_table "family_curators", :force => true do |t|
    t.string   "names"
    t.string   "kid_ages"
    t.string   "picture_string"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.text     "summary"
    t.text     "details"
  end

  create_table "faqs", :force => true do |t|
    t.string   "question",   :default => ""
    t.text     "answer",     :default => ""
    t.boolean  "visible",    :default => false
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  add_index "faqs", ["visible"], :name => "index_faqs_on_visible"

  create_table "favourite_kid_products", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "favourite_playgroundships", :force => true do |t|
    t.integer  "nyc_playground_id"
    t.integer  "favourite_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "favourite_restaurantships", :force => true do |t|
    t.integer  "favourite_id"
    t.integer  "nyc_restaurant_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "favourite_spotships", :force => true do |t|
    t.integer  "nyc_spot_id"
    t.integer  "favourite_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "favourites", :force => true do |t|
    t.string   "kid_products"
    t.string   "nyc_restaurants"
    t.string   "nyc_playgrounds"
    t.string   "nyc_spots"
    t.integer  "user_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "feeds", :force => true do |t|
    t.string   "title"
    t.text     "content"
    t.boolean  "visible",    :default => false
    t.datetime "visible_at"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  add_index "feeds", ["visible"], :name => "index_feeds_on_visible"

  create_table "friendly_id_slugs", :force => true do |t|
    t.string   "slug",                         :null => false
    t.integer  "sluggable_id",                 :null => false
    t.string   "sluggable_type", :limit => 40
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type"], :name => "index_friendly_id_slugs_on_slug_and_sluggable_type", :unique => true
  add_index "friendly_id_slugs", ["sluggable_id"], :name => "index_friendly_id_slugs_on_sluggable_id"
  add_index "friendly_id_slugs", ["sluggable_type"], :name => "index_friendly_id_slugs_on_sluggable_type"

  create_table "friendships", :force => true do |t|
    t.integer  "user_id"
    t.integer  "friend_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.string   "status"
    t.datetime "confirmed_at"
  end

  create_table "invitations", :force => true do |t|
    t.integer  "user_id",      :null => false
    t.string   "friend_email", :null => false
    t.text     "message"
    t.string   "token",        :null => false
    t.datetime "sent_at",      :null => false
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "invitations", ["token"], :name => "index_invitations_on_token"
  add_index "invitations", ["user_id"], :name => "index_invitations_on_user_id"

  create_table "kid_productships", :force => true do |t|
    t.integer  "favourite_kid_product_id"
    t.integer  "favourite_id"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "line_items", :force => true do |t|
    t.integer  "agegroup_id"
    t.integer  "user_subscription_id"
    t.float    "price"
    t.integer  "qty"
    t.integer  "package_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.boolean  "gift_it"
  end

  create_table "message_copies", :force => true do |t|
    t.integer  "sent_messageable_id"
    t.string   "sent_messageable_type"
    t.integer  "recipient_id"
    t.string   "subject"
    t.text     "body"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  add_index "message_copies", ["sent_messageable_id", "recipient_id"], :name => "outbox_idx"

  create_table "messages", :force => true do |t|
    t.integer  "received_messageable_id"
    t.string   "received_messageable_type"
    t.integer  "sender_id"
    t.string   "subject"
    t.text     "body"
    t.boolean  "opened",                    :default => false
    t.boolean  "deleted",                   :default => false
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
  end

  add_index "messages", ["received_messageable_id", "sender_id"], :name => "inbox_idx"

  create_table "messagings", :force => true do |t|
    t.integer  "sender_id"
    t.integer  "recipient_id"
    t.integer  "message_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.string   "status"
    t.string   "placed_as"
  end

  create_table "nyc_playgrounds", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "nyc_restaurants", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "nyc_spots", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "packages", :force => true do |t|
    t.string   "name"
    t.float    "price"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "trial_days"
    t.string   "details"
    t.boolean  "gift_it"
  end

  create_table "pages", :force => true do |t|
    t.string   "title"
    t.string   "slug"
    t.integer  "category_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.boolean  "hidden"
    t.text     "body"
    t.boolean  "show_title"
    t.boolean  "unpublished"
    t.integer  "author_id"
  end

  add_index "pages", ["slug"], :name => "index_pages_on_slug"

  create_table "products", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "brand_id"
    t.text     "description"
    t.string   "product_image"
    t.string   "facebook_page"
    t.string   "twitter_page"
    t.string   "web_page"
  end

  create_table "profiles", :force => true do |t|
    t.string   "title",          :default => ""
    t.string   "first_name",     :default => ""
    t.string   "last_name",      :default => ""
    t.string   "street_address", :default => ""
    t.string   "city",           :default => ""
    t.string   "state",          :default => ""
    t.string   "postal_code",    :default => ""
    t.string   "i_am",           :default => ""
    t.integer  "age",            :default => 0
    t.string   "hobbies",        :default => ""
    t.datetime "birthday"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
    t.string   "phone_cell"
    t.string   "phone_home"
    t.string   "display_name"
    t.integer  "user_id"
    t.string   "avatar"
    t.integer  "state_id"
    t.integer  "country_id"
  end

  create_table "providers", :force => true do |t|
    t.integer  "user_id",    :null => false
    t.string   "provider",   :null => false
    t.string   "uid",        :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "providers", ["provider"], :name => "index_providers_on_provider"
  add_index "providers", ["uid"], :name => "index_providers_on_uid"
  add_index "providers", ["user_id"], :name => "index_providers_on_user_id"

  create_table "recipient_details", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.text     "address"
    t.integer  "user_subscription_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.integer  "country_id"
    t.integer  "state_id"
    t.string   "postal_code"
    t.string   "final_address"
    t.string   "city"
    t.text     "message"
    t.string   "delivery"
  end

  create_table "settings", :force => true do |t|
    t.string   "our_story",          :default => ""
    t.string   "contact_us",         :default => ""
    t.string   "how_kid_boxes_work", :default => ""
    t.string   "privacy_policy",     :default => ""
    t.string   "terms_of_service",   :default => ""
    t.string   "facebook_page",      :default => ""
    t.string   "twitter_page",       :default => ""
    t.string   "home_page",          :default => ""
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.string   "pinterest_page"
    t.text     "partner_brands"
  end

  create_table "spouses", :force => true do |t|
    t.integer  "user_id",    :default => 0, :null => false
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
    t.string   "email"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "birthday"
  end

  create_table "states", :force => true do |t|
    t.string   "name"
    t.integer  "country_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "subscriptions", :force => true do |t|
    t.string   "subscription"
    t.integer  "duration"
    t.integer  "price"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "tiny_prints", :force => true do |t|
    t.string   "image_file_name"
    t.string   "image_file_size"
    t.string   "image_content_type"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "tiny_videos", :force => true do |t|
    t.string   "original_file_name"
    t.string   "original_file_size"
    t.string   "original_content_type"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "user_subscriptions", :force => true do |t|
    t.integer  "user_id"
    t.integer  "subscription_id"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.string   "stripe_customer_token"
    t.integer  "agegroup_id"
    t.integer  "box_id"
    t.integer  "package_id"
    t.float    "total"
    t.text     "shipping_address"
    t.float    "discount"
    t.text     "billing_address"
    t.string   "zip_code"
    t.string   "contact_no"
    t.boolean  "paymentstatus",          :default => false
    t.string   "event_token"
    t.string   "initial_customer_token"
    t.string   "card_token"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "",     :null => false
    t.string   "encrypted_password",     :default => "",     :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "password_salt"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        :default => 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "authentication_token"
    t.string   "invitation_token"
    t.string   "username"
    t.string   "role",                   :default => "user"
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
    t.integer  "invitation_id"
  end

  add_index "users", ["authentication_token"], :name => "index_users_on_authentication_token", :unique => true
  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["invitation_id"], :name => "index_users_on_invitation_id"
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["unlock_token"], :name => "index_users_on_unlock_token", :unique => true

end
