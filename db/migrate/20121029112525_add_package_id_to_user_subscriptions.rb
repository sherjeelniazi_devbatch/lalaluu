class AddPackageIdToUserSubscriptions < ActiveRecord::Migration
  def change
    add_column :user_subscriptions, :package_id, :integer
  end
end
