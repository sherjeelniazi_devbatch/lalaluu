class AddPartnerBrandsToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :partner_brands, :text
  end
end
