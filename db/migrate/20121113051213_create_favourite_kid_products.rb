class CreateFavouriteKidProducts < ActiveRecord::Migration
  def change
    create_table :favourite_kid_products do |t|
      t.string :name

      t.timestamps
    end
  end
end
