class CreateMessagings < ActiveRecord::Migration
  def change
  	create_table :messagings do |t|
  		t.integer :sender_id
  		t.integer :recipient_id
  		t.integer :message_id

  		t.timestamps
  	end
  end
end
