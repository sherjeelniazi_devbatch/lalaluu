class CreateFeeds < ActiveRecord::Migration
  def change
  	create_table :feeds do |t|
  		t.string :title
  		t.text :content
  		t.boolean :visible, default: false
  		t.datetime :visible_at
  		
  		t.timestamps
  	end
    add_index :feeds, :visible
  end
end