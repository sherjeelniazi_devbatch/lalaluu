class CreateFavouriteRestaurantships < ActiveRecord::Migration
  def self.up
    create_table :favourite_restaurantships do |t|
      t.integer :favourite_id
      t.integer :nyc_restaurant_id

      t.timestamps
    end
  end

  def self.down
  	drop_table :favourite_restaurantships
  end

end
