class AddZipCodeAndContactNoToUserSubscriptions < ActiveRecord::Migration
  def change
    add_column :user_subscriptions, :zip_code, :string
    add_column :user_subscriptions, :contact_no, :string
  end
end
