class CreateProviders < ActiveRecord::Migration
  def change
    create_table :providers do |t|
      t.integer :user_id,   :null => false
      t.string  :provider,  :null => false
      t.string  :uid,       :null => false

      t.timestamps
    end
		add_index :providers, 	:user_id
		add_index :providers, 	:provider
		add_index :providers, 	:uid
  end
end
