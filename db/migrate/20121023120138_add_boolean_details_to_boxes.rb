class AddBooleanDetailsToBoxes < ActiveRecord::Migration
  def change
    add_column :boxes, :active, :boolean
    add_column :boxes, :offer, :boolean
  end
end
