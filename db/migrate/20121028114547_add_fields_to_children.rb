class AddFieldsToChildren < ActiveRecord::Migration
  def change
  	add_column :children, :name, :string
  	add_column :children, :birthday, :datetime
  end
end
