class AddStateAndCountryToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :state_id, :integer
    add_column :profiles, :country_id, :integer
  end
end
