class CreateCityScopes < ActiveRecord::Migration
  def change
    create_table :city_scopes do |t|
      t.string :image_url
      t.string :redirect_url
      t.string :upload_file_name
      t.string :upload_content_type
      t.integer :upload_file_size
      t.datetime :upload_updated_at
      t.timestamps
    end
  end
  def down
    drop_table :city_scopes
  end
end
