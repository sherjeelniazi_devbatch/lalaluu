class CreateNycPlaygrounds < ActiveRecord::Migration
  def change
    create_table :nyc_playgrounds do |t|
      t.string :name

      t.timestamps
    end
  end
end
