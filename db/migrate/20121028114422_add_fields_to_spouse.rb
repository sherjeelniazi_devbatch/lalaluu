class AddFieldsToSpouse < ActiveRecord::Migration
  def change
  	add_column :spouses, :email, :string
  	add_column :spouses, :first_name, :string
  	add_column :spouses, :last_name, :string
  	add_column :spouses, :birthday, :string
  end
end
