class AddBillingAddressToUserSubscriptions < ActiveRecord::Migration
  def change
    add_column :user_subscriptions, :billing_address, :text
  end
end
