class AddDetailsToBoxes < ActiveRecord::Migration
  def change
    add_column :boxes, :box_image, :string
    add_column :boxes, :description, :text
    add_column :boxes, :age_group, :string
  end
end
