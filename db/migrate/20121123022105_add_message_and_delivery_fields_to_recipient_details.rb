class AddMessageAndDeliveryFieldsToRecipientDetails < ActiveRecord::Migration
  def change
    add_column :recipient_details, :message, :text
    add_column :recipient_details, :delivery, :string
  end
end
