class CreateRecipientDetails < ActiveRecord::Migration
  def change
    create_table :recipient_details do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.text :address
      t.integer :user_subscription_id

      t.timestamps
    end
  end
end
