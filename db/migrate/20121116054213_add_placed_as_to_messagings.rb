class AddPlacedAsToMessagings < ActiveRecord::Migration
  def change
    add_column :messagings, :placed_as, :string
  end
end
