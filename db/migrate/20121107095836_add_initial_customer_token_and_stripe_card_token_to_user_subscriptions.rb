class AddInitialCustomerTokenAndStripeCardTokenToUserSubscriptions < ActiveRecord::Migration
  def change
    add_column :user_subscriptions, :initial_customer_token, :string
    add_column :user_subscriptions, :card_token, :string
  end
end
