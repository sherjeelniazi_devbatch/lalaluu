class AddAgegroupIdToUserSubscriptions < ActiveRecord::Migration
  def change
    add_column :user_subscriptions, :agegroup_id, :integer
    add_column :user_subscriptions, :box_id, :integer
  end
end
