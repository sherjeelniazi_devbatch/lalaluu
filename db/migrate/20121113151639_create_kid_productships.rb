class CreateKidProductships < ActiveRecord::Migration
  def change
    create_table :kid_productships do |t|
      t.integer :favourite_kid_product_id
      t.integer :favourite_id

      t.timestamps
    end
  end
end
