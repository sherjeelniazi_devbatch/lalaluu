class CreateChildren < ActiveRecord::Migration
  def change
  	create_table(:children) do |t|
		  t.integer :user_id, :default => '', :null => false

		  t.timestamps
		end
		add_index :children, :user_id
  end
end
