class ChangeSexToIAmInProfile < ActiveRecord::Migration
  def up
  	rename_column :profiles, :sex, :i_am
  end

  def down
  	rename_column :profiles, :i_am, :sex
  end
end
