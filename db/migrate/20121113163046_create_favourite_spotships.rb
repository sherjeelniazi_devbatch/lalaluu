class CreateFavouriteSpotships < ActiveRecord::Migration
  def change
    create_table :favourite_spotships do |t|
      t.integer :nyc_spot_id
      t.integer :favourite_id

      t.timestamps
    end
  end
end
