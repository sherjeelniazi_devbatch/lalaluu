class AddStripeToUserSubscriptions < ActiveRecord::Migration
  def change
    add_column :user_subscriptions, :stripe_customer_token, :string
  end
end
