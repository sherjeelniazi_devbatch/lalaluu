class CreateBoxes < ActiveRecord::Migration
  def change
    create_table :boxes do |t|
      t.string :name
      t.float :price
      t.date :start_date
      t.date :end_date

      t.timestamps
    end
  end
end
