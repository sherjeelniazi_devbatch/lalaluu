class CreateInvitations < ActiveRecord::Migration
  def change
  	create_table 'invitations' do |t|
  		t.integer :user_id, null: false
  		t.string 	:friend_email, null: false
  		t.text 		:message
  		t.string	:token, null: false
  		t.datetime :sent_at, null: false

  		t.timestamps
  	end

  	add_index :invitations, :user_id
  	add_index :invitations, :token
  end
end
