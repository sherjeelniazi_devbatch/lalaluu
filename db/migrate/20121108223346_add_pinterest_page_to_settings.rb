class AddPinterestPageToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :pinterest_page, :string
  end
end
