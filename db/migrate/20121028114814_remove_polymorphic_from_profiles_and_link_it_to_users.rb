class RemovePolymorphicFromProfilesAndLinkItToUsers < ActiveRecord::Migration
  def up
  	remove_column :profiles, :profile_for_id
  	remove_column :profiles, :profile_for_type
  	add_column 		:profiles, :user_id, :integer
  end

  def down
  end
end
