class AddStateIdAndCountryIdToRecipientDetails < ActiveRecord::Migration
  def change
    add_column :recipient_details, :country_id, :integer
    add_column :recipient_details, :state_id, :integer
  end
end
