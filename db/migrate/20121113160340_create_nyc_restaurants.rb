class CreateNycRestaurants < ActiveRecord::Migration
  def self.up
    create_table :nyc_restaurants do |t|
      t.string :name

      t.timestamps
    end
  end
  
  def self.down
  	drop_table :nyc_restaurants
  end  

end
