class CreateSpouses < ActiveRecord::Migration
  def change
    create_table :spouses do |t|
    	t.integer :user_id, default: '', null: false

      t.timestamps
    end
  end
end
