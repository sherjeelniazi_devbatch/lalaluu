class CreateSettings < ActiveRecord::Migration
  def change
  	create_table :settings do |t|
  		t.string :our_story, 					:default => ''
  		t.string :contact_us, 				:default => ''
  		t.string :how_kid_boxes_work, :default => ''
  		t.string :privacy_policy, 		:default => ''
  		t.string :terms_of_service,		:default => ''
  		t.string :facebook_page,			:default => ''
  		t.string :twitter_page, 			:default => ''
  		t.string :home_page, 					:default => ''
  		
		  t.timestamps
  	end
  end
end
