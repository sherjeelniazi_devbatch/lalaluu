class ChangeHomeToString < ActiveRecord::Migration
  def up
  	remove_column :profiles, :phone_cell
  	remove_column :profiles, :phone_home
  	add_column :profiles, :phone_cell, :string
  	add_column :profiles, :phone_home, :string
  end

  def down
  	remove_column :profiles, :phone_cell
  	remove_column :profiles, :phone_home
  	add_column :profiles, :phone_cell, :integer
  	add_column :profiles, :phone_home, :integer
  end
end
