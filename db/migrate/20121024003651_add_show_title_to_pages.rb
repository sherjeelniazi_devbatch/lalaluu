class AddShowTitleToPages < ActiveRecord::Migration
  def change
    add_column :pages, :show_title, :boolean
    add_column :pages, :unpublished, :boolean
  end
end
