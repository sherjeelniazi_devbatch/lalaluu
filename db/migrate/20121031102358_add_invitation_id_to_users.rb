class AddInvitationIdToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :invitation_id, :integer
  	add_index :users, :invitation_id
  end
end
