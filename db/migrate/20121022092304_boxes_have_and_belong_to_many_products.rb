class BoxesHaveAndBelongToManyProducts < ActiveRecord::Migration
  def up
    create_table   :boxes_products, :id => false do |t|
           t.references :box, :product
         end
  end

  def down
    drop_table :boxes_products
  end
end
