class AddStatusAndConfirmedAtToFriendships < ActiveRecord::Migration
  def change
    add_column :friendships, :status, :string
    add_column :friendships, :confirmed_at, :datetime
  end
end
