class AgegroupHasAndBelongsToManyBoxes < ActiveRecord::Migration
  def up
    create_table   :agegroups_boxes, :id => false do |t|
           t.references :agegroup, :box
         end
  end

  def down
    drop_table :agegroups_boxes
  end
end
