class AddAgegroupIdToBoxes < ActiveRecord::Migration
  def change
    add_column :boxes, :agegroup_id, :integer
  end
end
