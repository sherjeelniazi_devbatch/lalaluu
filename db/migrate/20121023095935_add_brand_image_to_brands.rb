class AddBrandImageToBrands < ActiveRecord::Migration
  def change
    add_column :brands, :brand_image, :string
  end
end
