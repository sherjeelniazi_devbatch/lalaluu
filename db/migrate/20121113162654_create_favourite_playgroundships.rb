class CreateFavouritePlaygroundships < ActiveRecord::Migration
  def change
    create_table :favourite_playgroundships do |t|
      t.integer :nyc_playground_id
      t.integer :favourite_id

      t.timestamps
    end
  end
end
