class CreateLineItems < ActiveRecord::Migration
  def change
    create_table :line_items do |t|
      t.integer :agegroup_id
      t.integer :user_subscription_id
      t.float :price
      t.integer :qty
      t.integer :package_id

      t.timestamps
    end
  end
end
