class AddDiscountedToUserSubscriptions < ActiveRecord::Migration
  def change
    add_column :user_subscriptions, :discount, :float
  end
end
