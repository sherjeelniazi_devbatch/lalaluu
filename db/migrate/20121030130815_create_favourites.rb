class CreateFavourites < ActiveRecord::Migration
  def change
  	create_table :favourites do |t|
  		t.string :kid_products
  		t.string :nyc_restaurants
  		t.string :nyc_playgrounds
  		t.string :nyc_spots
  		t.integer :user_id
  		
  		t.timestamps
  	end
  end
end
