class AddShippingAddressToUserSubscriptions < ActiveRecord::Migration
  def change
    add_column :user_subscriptions, :shipping_address, :text
  end
end
