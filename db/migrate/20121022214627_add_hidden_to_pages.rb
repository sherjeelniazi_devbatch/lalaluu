class AddHiddenToPages < ActiveRecord::Migration
  def change
    add_column :pages, :hidden, :boolean
  end

  def self.down
  	drop_column :page, :hidden
  end
end
