class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.string :subscription
      t.integer :duration
      t.integer :price
      t.timestamps
    end
  end
end
