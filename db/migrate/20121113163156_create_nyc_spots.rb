class CreateNycSpots < ActiveRecord::Migration
  def change
    create_table :nyc_spots do |t|
      t.string :name

      t.timestamps
    end
  end
end
