class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :title
      t.string :body
      t.string :slug
      t.integer :category_id

      t.timestamps
    end
  end
end
