class AddShowInBlogToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :show_in_blog, :boolean
  end
end
