class AddDetailsToPackages < ActiveRecord::Migration
  def change
    add_column :packages, :details, :string
  end
end
