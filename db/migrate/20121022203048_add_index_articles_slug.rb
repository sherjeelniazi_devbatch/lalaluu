class AddIndexArticlesSlug < ActiveRecord::Migration
  def up
  	add_index :pages, :slug
  end

  def down
  	drop_index :pages, :slug
  end
end
