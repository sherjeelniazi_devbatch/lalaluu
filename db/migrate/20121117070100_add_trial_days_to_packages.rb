class AddTrialDaysToPackages < ActiveRecord::Migration
  def change
    add_column :packages, :trial_days, :integer
  end
end
