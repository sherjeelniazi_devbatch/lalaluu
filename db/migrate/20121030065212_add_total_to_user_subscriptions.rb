class AddTotalToUserSubscriptions < ActiveRecord::Migration
  def change
    add_column :user_subscriptions, :total, :float
  end
end
