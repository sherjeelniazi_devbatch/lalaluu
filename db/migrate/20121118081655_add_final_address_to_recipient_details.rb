class AddFinalAddressToRecipientDetails < ActiveRecord::Migration
  def change
    add_column :recipient_details, :final_address, :string
  end
end
