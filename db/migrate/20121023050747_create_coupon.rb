class CreateCoupon < ActiveRecord::Migration
  def change
    create_table :coupons do |t|
    	t.string :name
      t.integer :discount, default: 0
    	t.boolean :available, default: false
    	t.integer :box_id

    	t.timestamps
    end
		add_index :coupons, 	:name
		add_index :coupons, 	:box_id
  end
end
