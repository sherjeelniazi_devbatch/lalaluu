class ChangeSummaryAndDetailsColumnFamilyCurator < ActiveRecord::Migration
  def up
  	remove_column :family_curators, :summary
	remove_column :family_curators, :details
	add_column :family_curators, :summary, :text
  	add_column :family_curators, :details, :text

  end

  def down
  	remove_column :family_curators, :summary
	remove_column :family_curators, :details
	add_column :family_curators, :summary, :string
  	add_column :family_curators, :details, :string
  end
end
