class AddEventTokenToUserSubscription < ActiveRecord::Migration
  def change
    add_column :user_subscriptions, :event_token, :string
  end
end
