class AddSocialFieldsProduct < ActiveRecord::Migration
  def up
    add_column :products , :facebook_page ,:string
    add_column :products , :twitter_page ,:string
    add_column :products , :web_page ,:string
  end

  def down
    remove_column :products , :facebook_page 
    remove_column :products , :twitter_page 
    remove_column :products , :web_page 
  end
end
