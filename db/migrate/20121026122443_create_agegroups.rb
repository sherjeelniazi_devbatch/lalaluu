class CreateAgegroups < ActiveRecord::Migration
  def change
    create_table :agegroups do |t|
      t.string :group_name
      t.integer :start_age
      t.integer :end_age
      t.string  :format
      t.timestamps
    end
  end
end
