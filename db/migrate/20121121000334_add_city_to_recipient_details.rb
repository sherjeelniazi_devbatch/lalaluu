class AddCityToRecipientDetails < ActiveRecord::Migration
  def change
    add_column :recipient_details, :city, :string
  end
end
