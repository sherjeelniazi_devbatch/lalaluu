class CreateFamilyCurators < ActiveRecord::Migration
  def change
    create_table :family_curators do |t|
      t.string :names
      t.string :kid_ages
      t.string :picture_string
      t.string :summary
      t.string :details

      t.timestamps
    end
  end
end
