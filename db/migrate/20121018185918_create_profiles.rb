class CreateProfiles < ActiveRecord::Migration
  def change
  	create_table(:profiles) do |t|
		  t.string	:title, 				:default => ""
		  t.string 	:first_name, 		:default => ""
		  t.string 	:last_name, 		:default => ""
		  t.string 	:street_address,:default => ""
		  t.string 	:city, 					:default => ""
		  t.string 	:state, 				:default => ""
		  t.string 	:postal_code, 	:default => ""
		  t.integer :phone_cell,		:default => ""
		  t.integer :phone_home,		:default => ""
		  t.string 	:sex,						:default => ""
		  t.integer :age,						:default => ""
		  t.string 	:hobbies,				:default => ""
		  t.datetime 	:birthday
		  t.references :profile_for, 	:polymorphic => true

		  t.timestamps
		end
		add_index :profiles, 	:profile_for_id
		add_index :profiles, 	:profile_for_type
  end
end
