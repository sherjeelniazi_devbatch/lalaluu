class CreateFaqs < ActiveRecord::Migration
  def change
  	create_table :faqs do |t|
  		t.string :question, :default => ''
  		t.string :answer,		:default => ''
  		t.boolean :visible, :default => false

		  t.timestamps
		end
		add_index :faqs, :visible
  end
end
