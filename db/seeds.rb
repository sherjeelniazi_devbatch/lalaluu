# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
ActiveRecord::Base.connection.execute("TRUNCATE packages")
Setting.create(:facebook_page => 'http://facebook.com/your-page-name')
Package.create! name: "1 Month", price: 30
Package.create! name: "3 Months", price: 75
Package.create! name: "6 Month", price: 125
Package.create! name: "12 Month", price: 200
